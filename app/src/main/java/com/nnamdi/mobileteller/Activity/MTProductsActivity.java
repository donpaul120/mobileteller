package com.nnamdi.mobileteller.Activity;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.Fragments.MTProductsFragment;
import com.nnamdi.mobileteller.Interfaces.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.POJO.Merchants.MTMerchant;
import com.nnamdi.mobileteller.POJO.Merchants.MTMerchantDataKey;
import com.nnamdi.mobileteller.POJO.Products.MTProduct;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

public class MTProductsActivity extends ActionBarActivity implements MTFragmentCommunicationInterface{

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_mtproducts);

            if(savedInstanceState==null)
        {
            MTProductsFragment fragment =  MTProductsFragment.newInstance(getIntent().getExtras());
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }

        setActivityToolbar();
    }

    public void setActivityToolbar()
    {
        Toolbar toolbar = MTGeneralActionBar.setGeneralActionBar(this, "MobileTeller", "Products");
        if(toolbar!=null)
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mtproducts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void triggerAction(String ActionType, int id, Bundle data) {

    }
}
