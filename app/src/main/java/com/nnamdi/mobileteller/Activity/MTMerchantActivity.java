package com.nnamdi.mobileteller.Activity;



import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import com.nnamdi.mobileteller.Components.MTDrawerLayout;
import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.ConnectionPool.MTRequestManager;
import com.nnamdi.mobileteller.Constants.Actions;
import com.nnamdi.mobileteller.Constants.MTConnectionConst;
import com.nnamdi.mobileteller.Fragments.MTMerchantFragment;
import com.nnamdi.mobileteller.Interfaces.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Interfaces.MTMerchantCallBackInterface;
import com.nnamdi.mobileteller.R;

public class MTMerchantActivity extends ActionBarActivity implements MTFragmentCommunicationInterface, MTMerchantCallBackInterface {


    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ImageView loadingFragmentIcon;
    private TextView connectionStatusView;
    private Button connectionRetryBtn;
    private Animation rotate;
    private MTMerchantFragment productsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchants);

        int listViewID = R.id.mobileTelListV;
        int drawerID = R.id.myDrawer;
        MTDrawerLayout MTDrawerLayout = new MTDrawerLayout(this,drawerID,listViewID);

        if(savedInstanceState==null){requestMerchantList();}

        actionBarDrawerToggle = MTDrawerLayout.getActionBarDrawerToggle();
        Toolbar toolbar = MTGeneralActionBar.setGeneralActionBar(this,"MobileTeller","Merchants");
        this.setSupportActionBar(toolbar);
        //Widget Setup
        setConnectionRetryBtn();
        setConnectionStatusView();

    }

    /**
     * MobileTeller
     * Paul Okeke
     * Triggers the background process to request
     * for list of merchants
     * @since 2014
     */
    public void requestMerchantList()
    {
        String actionType[] = {Actions.MERCHANT_ALL};
        MTRequestManager requestManager = new MTRequestManager();
        requestManager.setCallBackReceiver(this);
        requestManager.execute(actionType);
        startLoadingMerchantsAnim();
    }

    /**
     * MobileTeller
     */
    public void startLoadingMerchantsAnim()
    {
        loadingFragmentIcon = (ImageView) findViewById(R.id.load);
        loadingFragmentIcon.setVisibility(View.VISIBLE);
        rotate = AnimationUtils.loadAnimation(this, R.anim.mt_activity_loading);
        loadingFragmentIcon.startAnimation(rotate);
    }

    public void stopLoadingMerchantAnim()
    {
        rotate.cancel();
        loadingFragmentIcon.clearAnimation();
        loadingFragmentIcon.setVisibility(View.GONE);
    }

    public void setConnectionRetryBtn()
    {
        connectionRetryBtn = (Button) findViewById(R.id.conn_retry);
        connectionRetryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1, 0);
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.setDuration(500);
                connectionRetryBtn.startAnimation(animation);
                connectionStatusView.startAnimation(animation);
                connectionRetryBtn.setVisibility(View.GONE);
                connectionStatusView.setVisibility(View.GONE);
                requestMerchantList();
            }
        });
    }

    public void  setConnectionStatusView()
    {
        connectionStatusView = (TextView) findViewById(R.id.conn_stat);
    }

    /*public void startPullToRefreshAnim()
    {

    }

    public void stopPullToRefreshAnim()
    {

    }*/

    public void displayConnectionError()
    {
        //TODO: Do Custom Layout for Displaying Error
        stopLoadingMerchantAnim();
        Animation animation = new AlphaAnimation(0, 1);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(500);
        connectionStatusView.startAnimation(animation);
        connectionRetryBtn.startAnimation(animation);
        connectionStatusView.setVisibility(View.VISIBLE);
        connectionRetryBtn.setVisibility(View.VISIBLE);
    }

    /**
     * MobileTeller
     * @param dataBundle    Bundle that holds data for the fragment to handle
     *                      The bundle also contains connection details or status
     */
    public void addFragmentToActivity(Bundle dataBundle)
    {
        if(productsFragment==null) {
            productsFragment = MTMerchantFragment.newInstance(dataBundle);
            getFragmentManager().beginTransaction()
                    .add(R.id.container, productsFragment)
                    .commit();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView( menu.findItem(R.id.action_settings));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return actionBarDrawerToggle.onOptionsItemSelected(item)
                || id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void triggerAction(String ActionType,int id, Bundle data) {
        //Do Something here brother
        if(id !=89 )
        {
            Intent intent = new Intent(this, MTProductsActivity.class);
            intent.putExtras(data);
            this.startActivity(intent);
        }
    }

    @Override
    public void getData(Bundle data) {
        String connectionStat = data.getString(MTConnectionConst.CONNECTION_STATUS_KEY);

        if(!connectionStat.equals(MTConnectionConst.CONNECTION_FAILED) &&
                !connectionStat.equals(MTConnectionConst.CONNECTION_REFUSED)) {

            addFragmentToActivity(data);
            stopLoadingMerchantAnim();
        }
        else{
                displayConnectionError();
        }

    }
}
