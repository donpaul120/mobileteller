package com.nnamdi.mobileteller.Factory.ViewFactory;

/**
 * @author Paul Okeke
 * Created by paulex10 on 22/01/2015.
 */
public class StyleConstants {

    /**
     * Note that the tff files should exist within your assets directory
     */
    public static String GENERAL_APP_FONT = "OpenSans-Regular.ttf";//The file name of a font in the assets directory
    public static String GENERAL_APP_FONT_BOLD = "OpenSans-Semibold.ttf";//The file name of a font in the assets directory
    public static String BOLD_ITALIC_FONT = "OpenSans-BoldItalic.ttf";


    /*You can list as much as you have in you asset directory and reference any when setting a font for a view/views*/
}
