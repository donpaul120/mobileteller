package com.nnamdi.mobileteller.Products.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Components.GeneralDialog;
import com.nnamdi.mobileteller.Components.Interface.GeneralDialogInterface;
import com.nnamdi.mobileteller.Constants.ActionType;
import com.nnamdi.mobileteller.Constants.AuthConstant;
import com.nnamdi.mobileteller.Constants.DialogType;
import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.HBSessionManager;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Products.Adapter.ProductDetailsAdapter;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;
import com.nnamdi.mobileteller.Products.Utility.ProductDetail;
import com.nnamdi.mobileteller.Products.Utility.ProductVariant;
import com.nnamdi.mobileteller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Paul Okeke
 * Project Leader Nnamdi Jibunoh
 * @since 2014
 * 21/11/2014
 */
public class MTProductDetailFragment extends Fragment implements GeneralDialogInterface{

    public final static String TAG = "DETAIL_FRAGMENT";
    private MTFragmentCommunicationInterface mListener;
    private ExpandableListView detailListView;
    private ArrayList<ProductDetail> listItem;
    private Context context;
    private TextView headerTitle;
    private int color;
    private ImageButton addToCart;
    private Button checkOut;
    private Bundle bundle;

    private String productID;
    private boolean isInCart;

    private HBSessionManager sessionManager;

    private JSONArray cartArgs;

    private SharedPreferences sharedPref;


    public MTProductDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
        bundle = new Bundle();
        getProductsInCart();
    }

    public void getProductsInCart(){
        try{
            sessionManager = new HBSessionManager(context);
            sharedPref = sessionManager.getSharedPref();
            String cartSArray = sharedPref.getString("CART","[]");
            cartArgs = new JSONArray(cartSArray);
        }catch (JSONException ex){
            ex.printStackTrace();
        }
    }

    public static MTProductDetailFragment newInstance(MTProduct product){
        MTProductDetailFragment detailFragment = new MTProductDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("PRODUCT_DETAILS", product);
        detailFragment.setArguments(args);
        return detailFragment;
    }

    public void doDetailList(){
        MTProduct product = getArguments().getParcelable("PRODUCT_DETAILS");
        if(product!=null) {
            ProductDetail productNameGroup = new ProductDetail();
            productNameGroup.setDetailName(product.getProductName());
            productNameGroup.setGroupLabelName("Product Name");
            productNameGroup.setGroupIcon(0);
            productNameGroup.setHasLabel(true);
            productNameGroup.setFontSize(18);
            listItem.add(productNameGroup);

            ProductDetail productSalesPriceGroup = new ProductDetail();
            productSalesPriceGroup.setDetailName(""+product.getProductSalesPrice());
            productSalesPriceGroup.setGroupLabelName("Price");
            productSalesPriceGroup.setGroupIcon(1);
            productSalesPriceGroup.setHasLabel(true);
            productSalesPriceGroup.setFontSize(18);
            listItem.add(productSalesPriceGroup);

            ProductDetail productDescriptionGroup = new ProductDetail();
            productDescriptionGroup.setDetailName("" +product.getProductDescription());
            productDescriptionGroup.setGroupLabelName("Description");
            productDescriptionGroup.setGroupIcon(0);
            productDescriptionGroup.setHasLabel(true);
            productDescriptionGroup.setFontSize(15);
            listItem.add(productDescriptionGroup);

            //for variants
            doProductVariants(product.getProductVariantsJson());

            headerTitle.setText(product.getProductName());
            this.productID = product.getProductID();
        }
    }

    public void doProductVariants(String variantsJson){
        try{
            JSONArray variantsArgs = new JSONArray(variantsJson);
            for(int i=0;i<variantsArgs.length();i++){
                ProductDetail productDetail = new ProductDetail();
                JSONObject variantObj = variantsArgs.getJSONObject(i);
                productDetail.setGroupLabelName(variantObj.getString("category_name"));
                productDetail.setHasLabel(true);
                productDetail.setFontSize(18);
                productDetail.setHasVariant(true);
                JSONArray variantDetailsArgs = variantObj.getJSONArray("product_variant_details");
                HashMap<String, ProductVariant> variants = new HashMap<>();
                for(int j=0;j<variantDetailsArgs.length();j++){
                    ProductVariant node = new ProductVariant();
                    JSONObject variantDetailObj = variantDetailsArgs.getJSONObject(j);
                    String key = variantDetailObj.getString("sub_category_name");
                    node.setVariantSubCategory(variantDetailObj.getString("sub_category_name"));
                    node.setSellingPrice(variantDetailObj.getInt("selling_price"));
                    variants.put(key,node);
                }
                productDetail.setVariants(variants);
                listItem.add(productDetail);
            }
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    public boolean isProductInCart(){
        boolean inCart= false;
        if(cartArgs.length()==0){
            inCart = false;
        }else {
            try {
                for (int i = 0; i < cartArgs.length(); i++) {
                    JSONObject productObj = cartArgs.getJSONObject(i);
                    if(productObj.has(productID)){
                        inCart = true;
                        break;
                    }
                }
            }catch (JSONException ex){
                ex.printStackTrace();
            }
        }
        return inCart;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mtproduct_detail, container, false);
        detailListView = (ExpandableListView)rootView.findViewById(R.id.product_detail_list);
        detailListView.setGroupIndicator(context.getResources().getDrawable(R.drawable.group_indicator_bg));
        View headerView = LayoutInflater.from(context).inflate(R.layout.product_detail_header_view, detailListView, false);
        setHeaderToFitDevice(headerView);
        View footerView =  LayoutInflater.from(context).inflate(R.layout.product_details_footer_view, detailListView, false);
        headerTitle = (TextView) headerView.findViewById(R.id.product_name);
        FontStyleFactory.setFontForView(context, headerTitle, StyleConstants.GENERAL_APP_FONT);
        setAddToCart((ImageButton) headerView.findViewById(R.id.add_to_cart));
        setCheckOut((Button) footerView.findViewById(R.id.check_out_btn));
        detailListView.addHeaderView(headerView);
        detailListView.addFooterView(footerView);
        // Inflate the layout for this fragment
        return rootView;
    }

    public void setHeaderToFitDevice(View headerView){
        RelativeLayout mainVisibleHeader = (RelativeLayout)headerView.findViewById(R.id.visible_header_main);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        mainVisibleHeader.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height/2));
    }

    private void initializeListView(){
        if(listItem==null){
            listItem = new ArrayList<>();
            doDetailList();
        }
        detailListView.setAdapter(new ProductDetailsAdapter(context, listItem));
        detailListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if(i==0){
                    mListener.triggerAction(ActionType.LIST_SCROLL, i, null);
                }else if(i>0){
                    mListener.triggerAction(ActionType.LIST_SCROLL, i, null);
                }
            }
        });
        if (isProductInCart()){
            productIsInCart();
        }else{
            productNotInCart();
        }
    }

    public void initializeInfoDialog(Bundle bundle, String dialogType)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView  = inflater.inflate(R.layout.hb_auth_info_dialog, null);
        GeneralDialog confirmDialog = new GeneralDialog(context, dialogView, detailListView.getRootView(), bundle);
        confirmDialog.setDialogType(dialogType);
        confirmDialog.setDialogInterface(this);
        if(dialogType.equalsIgnoreCase(DialogType.ADD_TO_CART))
            confirmDialog.setPrimaryBtnTitle("ADD");
        else if(dialogType.equalsIgnoreCase(DialogType.REMOVE_FROM_CART))
            confirmDialog.setPrimaryBtnTitle("REMOVE");

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("IS_IN_CART", isInCart);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            isInCart = savedInstanceState.getBoolean("IS_IN_CART");
            if(isInCart){
                onProductAddedToCart();
            }else{
                onProductRemovedFromCart();
            }
        }
        if(savedInstanceState==null){
           // getProductsInCart();
        }
        initializeListView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MTFragmentCommunicationInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onOkay(String dialogType) {
        if(dialogType.equalsIgnoreCase(DialogType.ADD_TO_CART)) {
            Toast.makeText(context, " " + headerTitle.getText().toString() + " has been added to your cart", Toast.LENGTH_SHORT).show();
            onProductAddedToCart();
            final MediaPlayer mp = MediaPlayer.create(context, R.raw.add_to_cart_sound);
            mp.setVolume(5,5);
            mp.start();
            detailListView.smoothScrollToPosition(detailListView.getAdapter().getCount()-1);
        }
        else if(dialogType.equalsIgnoreCase(DialogType.REMOVE_FROM_CART)){
            Toast.makeText(context, " " + headerTitle.getText().toString() + " has been removed from your cart", Toast.LENGTH_SHORT).show();
            onProductRemovedFromCart();
        }
    }

    public void onProductAddedToCart(){
        //add the product to the cart
        try {
            JSONObject productObj = new JSONObject();
            JSONObject productDetails = new JSONObject();
            productDetails.put("product_name", headerTitle.getText().toString());
            productObj.put(productID, productDetails);
            cartArgs.put(productObj);
            sessionManager.put("CART", cartArgs.toString());
            Log.e("ADDED CART", cartArgs.toString());
        }catch (JSONException jex){
            jex.printStackTrace();
        }
        productIsInCart();
    }

    public void productIsInCart(){
        checkOut.setBackgroundResource(R.drawable.mt_button_drawable_bg);
        checkOut.setClickable(true);
        addToCart.setBackgroundResource(R.drawable.remove_from_cart_header_btn);
        addToCart.setImageResource(R.drawable.remove_cart_icon);
        isInCart = true;
    }

    public void onProductRemovedFromCart(){
        //add the product to the cart
        JSONArray newCart = new JSONArray();
        try{
            for (int i =0;i<cartArgs.length();i++){
                JSONObject productObj = cartArgs.getJSONObject(i);
                if(!productObj.has(productID)){
                    newCart.put(productObj);
                }
            }
            cartArgs = newCart;
            sessionManager.put("CART", cartArgs.toString());
            Log.e("REMOVED CART", cartArgs.toString());
        }catch (JSONException ex){
            ex.printStackTrace();
        }
        productNotInCart();
    }

    public void productNotInCart(){
        checkOut.setBackgroundColor(Color.parseColor("#EEEEEE"));
        checkOut.setClickable(false);
        addToCart.setBackgroundResource(R.drawable.product_detail_header_btn_m);
        addToCart.setImageResource(R.drawable.cart_icon_red_small);
        FontStyleFactory.setFontForView(context, checkOut, StyleConstants.GENERAL_APP_FONT);
        isInCart = false;
    }

    @Override
    public void onCancel() {

    }

    public void setAddToCart(ImageButton addToCart) {
        this.addToCart = addToCart;
        this.addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isInCart) {
                    bundle.putString(AuthConstant.AUTH_DIALOG_TITLE, AuthConstant.ADD_TO_CART);
                    bundle.putString(AuthConstant.AUTH_DIALOG_CONTENT, AuthConstant.ADD_TO_CART_MESSAGE);
                    initializeInfoDialog(bundle, DialogType.ADD_TO_CART);
                }else{
                    bundle.putString(AuthConstant.AUTH_DIALOG_TITLE, AuthConstant.REMOVE_FROM_CART);
                    bundle.putString(AuthConstant.AUTH_DIALOG_CONTENT, AuthConstant.REMOVE_FROM_CART_MESSAGE);
                    initializeInfoDialog(bundle, DialogType.REMOVE_FROM_CART);
                }
            }
        });
    }

    public void setCheckOut(Button checkOut) {
        this.checkOut = checkOut;
        FontStyleFactory.setFontForView(context, this.checkOut, StyleConstants.GENERAL_APP_FONT);
    }
}