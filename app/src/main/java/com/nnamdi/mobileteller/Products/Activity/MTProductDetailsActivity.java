package com.nnamdi.mobileteller.Products.Activity;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.Constants.ActionType;
import com.nnamdi.mobileteller.Products.Fragment.MTProductDetailFragment;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

public class MTProductDetailsActivity extends AppCompatActivity implements MTFragmentCommunicationInterface {

    private Toolbar toolbar;
    private TextView toolBarText;
    private MTProductDetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtproduct_details);

        if(savedInstanceState==null)
        {
            FragmentManager manager = getSupportFragmentManager();
            detailFragment = (MTProductDetailFragment) manager.findFragmentByTag(MTProductDetailFragment.TAG);
            if(detailFragment==null) {
                MTProduct product =  getIntent().getParcelableExtra("PRODUCT_DETAILS");
                detailFragment = MTProductDetailFragment.newInstance(product);
                manager.beginTransaction()
                        .add(R.id.container, detailFragment, MTProductDetailFragment.TAG)
                        .commit();
            }
        }
        toolbar = MTGeneralActionBar.setGeneralActionBar(this, "", "");
        if(toolbar!=null){
            toolbar.bringToFront();
            toolbar.setBackgroundColor(Color.TRANSPARENT);
            toolBarText = (TextView) toolbar.findViewById(R.id.mt_toolbar_title);
        }
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mtproduct_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }


    @Override
    public void triggerAction(int actionType, int id, Bundle data) {
        if(actionType == ActionType.LIST_SCROLL){
            if(id==0){
            //thats we are at the top
//                toolbar.setBackgroundColor(Color.parseColor("#2196F3"));
                toolBarText.setText("");
                toolBarText.invalidate();
            }else{
                //toolbar.bringToFront();
//                toolbar.setBackgroundResource(R.drawable.food1);
//                toolBarText.setText("Chicken Republic Text");
                toolBarText.invalidate();
            }
        }
    }
}