package com.nnamdi.mobileteller.Products.Utility;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Paul Okeke
 * Created by paulex10 on 12/11/2014.
 */
public class MTProduct implements Parcelable{

    private String productID;
    private String productName;
    private String productManufacturer;
    private int productTimeToPrepare;
    private int productPrice;
    private int productSalesPrice;
    private int productFreeShipping;
    private int productTaxable;
    private String productDescription;
    private String productImages;
    private String productCategoryName;
    private String productVariantsJson;

    /**
     *
     * Default Constructor for Product
     */
    public MTProduct()
    {}

    public void setProductID(String productID)
    {
        this.productID = productID;
    }

    public String getProductID()
    {
        return productID;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductTimeToPrepare(int productTimeToPrepare) {
        this.productTimeToPrepare = productTimeToPrepare;
    }

    public int getProductTimeToPrepare() {
        return productTimeToPrepare;
    }

    public void setProductPrice(int productPrice){
        this.productPrice = productPrice;
    }

    public double getProductPrice(){
        return productPrice;
    }

    public void setProductSalesPrice(int productSalesPrice) {
        this.productSalesPrice = productSalesPrice;
    }

    public int getProductSalesPrice() {
        return productSalesPrice;
    }

    public void setProductFreeShipping(int productFreeShipping) {
        this.productFreeShipping = productFreeShipping;
    }

    public int getProductFreeShipping() {
        return productFreeShipping;
    }

    public void setProductTaxable(int productTaxable) {
        this.productTaxable = productTaxable;
    }

    public int getProductTaxable() {
        return productTaxable;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public String getProductImages() {
        return productImages;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    /**
     * This constructor is called by the Creator Class
     * @param parcel    parcel what makes up the product part
     *
    */
    private MTProduct(Parcel parcel)
    {
        this.productID = parcel.readString();
        this.productName = parcel.readString();
        this.productPrice = parcel.readInt();
        this.productDescription = parcel.readString();
        this.productImages = parcel.readString();
        this.productCategoryName = parcel.readString();
        this.productVariantsJson = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(productID);
        destination.writeString(productName);
        destination.writeInt(productPrice);
        destination.writeString(productDescription);
        destination.writeString(productImages);
        destination.writeString(productCategoryName);
        destination.writeString(productVariantsJson);
    }

    public static Creator<MTProduct> CREATOR = new Creator<MTProduct>() {
        @Override
        public MTProduct createFromParcel(Parcel source) {

            return new MTProduct(source);
        }

        @Override
        public MTProduct[] newArray(int size) {
            return new MTProduct[size];
        }
    };

    public String getProductVariantsJson() {
        return productVariantsJson;
    }

    public void setProductVariantsJson(String productVariantsJson) {
        this.productVariantsJson = productVariantsJson;
    }
}
