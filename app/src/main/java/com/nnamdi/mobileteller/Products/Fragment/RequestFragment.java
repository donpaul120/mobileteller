package com.nnamdi.mobileteller.Products.Fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;

import com.nnamdi.mobileteller.ConnectionPool.MTRequestManager;
import com.nnamdi.mobileteller.Constants.Actions;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;

import java.util.ArrayList;

/**
 * @author  Paul Okeke
 * Created by paulex10 on 17/06/2015.
 */
public class RequestFragment extends Fragment implements ItemsListCallBackInterface {

    public static String TAG = "REQUEST_FRAGMENT";
    private ItemsListCallBackInterface ItemsListCallBackInterface;
    public LruCache<String, Bitmap> bitmapLruCache;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    public void requestMerchantsByLocation(){
        MTRequestManager requestManager = new MTRequestManager(this);
        String actionType[] = {Actions.MERCHANT_ALL};
        requestManager.execute(actionType);
    }

    public void requestProductsByCategory(String merchantId, String categoryId){
        MTRequestManager requestManager = new MTRequestManager(this);
        String params[] = {Actions.MERCHANT_CATEGORY_PRODUCTS, merchantId, categoryId };
        requestManager.execute(params);
    }


    public void requestCategoryProductsCount(String merchantId){
        MTRequestManager requestManager = new MTRequestManager(this);
        String actionType[] = {Actions.CATEGORY_PRODUCTS_COUNT, merchantId};
        requestManager.execute(actionType);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            ItemsListCallBackInterface = (ItemsListCallBackInterface) activity;
        }catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        ItemsListCallBackInterface = null;
    }

    @Override
    public void onConnectionFailed() {
        //check for null pointer just incase the fragment has being detached
        if(ItemsListCallBackInterface !=null) {
            ItemsListCallBackInterface.onConnectionFailed();
        }
    }

    @Override
    public void onConnectionSuccessful(ArrayList<?> items) {
        if(ItemsListCallBackInterface != null) {
            ItemsListCallBackInterface.onConnectionSuccessful(items);
        }
    }
}
