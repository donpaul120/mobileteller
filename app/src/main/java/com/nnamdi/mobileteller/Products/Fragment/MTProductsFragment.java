package com.nnamdi.mobileteller.Products.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;


import com.nnamdi.mobileteller.Components.MTListView;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Products.Activity.MTProductDetailsActivity;
import com.nnamdi.mobileteller.Products.Adapter.MTProductListAdapter;
import com.nnamdi.mobileteller.Components.MTMerchantInfoDialog;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;
import com.nnamdi.mobileteller.Products.MTProductDataKey;
import com.nnamdi.mobileteller.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class MTProductsFragment extends Fragment implements AbsListView.OnItemClickListener , ItemsListCallBackInterface {



    private String NO_PRODUCTS_KEY = "NO_PRODUCTS_KEY";


    private MTFragmentCommunicationInterface mListener;
    private ImageCacheInterface cacheInterface;
    private Activity context;
    private View rootView;
    private PopupWindow merchantInfoWindow;
    private boolean mtPopUpClick = true;
    private ArrayList<String> productDetails;

    public static String TAG = "MT_PRODUCTS_FRAGMENT";


    private ListView mListView;
    private TextView noProductsInCategory;
    private ArrayList<MTProduct> products;

    //private Context context;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private MTProductListAdapter mAdapter;

    public static MTProductsFragment newInstance(Bundle productBundle) {
        MTProductsFragment fragment = new MTProductsFragment();
        fragment.setArguments(productBundle);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MTProductsFragment() {
    }

    public static MTProductsFragment newInstance(){
        return new MTProductsFragment();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(products!=null){
            outState.putParcelableArrayList(MTProductDataKey.PRODUCTS, products);
        }if(noProductsInCategory!=null){
            outState.putInt(NO_PRODUCTS_KEY, noProductsInCategory.getVisibility());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getActivity();
    }

    public void initializeListView(){
        if(products == null){
            products = new ArrayList<>();

        }
        productDetails = new ArrayList<>(10);
        MTListView.dispatchScrollEvent(mListView, mListener);
        mAdapter = new MTProductListAdapter(context, products, cacheInterface);
        mListView.setAdapter(mAdapter);
    }

    public void displayNoProducts(boolean hasNoProducts){
        if(noProductsInCategory!=null) {
            if (hasNoProducts) {
                noProductsInCategory.setVisibility(View.VISIBLE);
            } else {
                noProductsInCategory.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        mListView = (ListView) rootView.findViewById(R.id.list);
        noProductsInCategory = (TextView) rootView.findViewById(R.id.no_products_in_cat);
        mListView.addHeaderView(LayoutInflater.from(context).inflate(R.layout.header_view, mListView, false));
        mListView.setOnItemClickListener(this);

        FontStyleFactory.setFontForView(context, noProductsInCategory, StyleConstants.GENERAL_APP_FONT_BOLD);

        return rootView;
    }

    public void initPopUp()
    {
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View lay = inflater.inflate(R.layout.mt_merchant_details_pop, null);
            Bundle merchantDetails = this.getArguments().getBundle(MTMerchantDataKey.MERCHANT);
            new MTMerchantInfoDialog(context.getApplicationContext(), merchantDetails, lay, rootView);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            products = savedInstanceState.getParcelableArrayList(MTProductDataKey.PRODUCTS);
            //noinspection ResourceType
            noProductsInCategory.setVisibility(savedInstanceState.getInt(NO_PRODUCTS_KEY));
        }
        initializeListView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MTFragmentCommunicationInterface) activity;
            cacheInterface = (ImageCacheInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {

            ImageView productImage = (ImageView) view.findViewById(R.id.merchant_main);
            saveProductClickImageToCache(((BitmapDrawable)productImage.getDrawable()).getBitmap());
            MTProduct product = (MTProduct) parent.getItemAtPosition(position);
            // productDetails.add(product.getProductName());
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Intent intent = new Intent(getActivity(), MTProductDetailsActivity.class);
            intent.putExtra("PRODUCT_DETAILS", product);
            startActivity(intent);
        }
    }

    public void saveProductClickImageToCache(Bitmap bitmap){
        File tempFile;
//        try{
////            tempFile = File.createTempFile("product_clicked", null, context.getCacheDir());
////            FileOutputStream oStream = new FileOutputStream(tempFile);
////            bitmap.compress(Bitmap.CompressFormat.JPEG,100, oStream);
//
////            oStream.close();
//        }catch(IOException ex){
//            ex.printStackTrace();
//        }
    }

    @Override
    public void onConnectionFailed() {
        displayNoProducts(false);
    }

    /**
     * This method will be triggered by the activity containing this fragment
     * after its loaded the data completely
     * @param products
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onConnectionSuccessful(ArrayList<?> products) {
        for (MTProduct product:(ArrayList<MTProduct>)products) {
            this.products.add(product);
        }
        mAdapter.notifyDataSetChanged();
        if(products.size()==0){
            displayNoProducts(true);
        }
        System.gc();
        Log.i("PRODUCT_FRAGMET", "I got the message from success");
    }
}