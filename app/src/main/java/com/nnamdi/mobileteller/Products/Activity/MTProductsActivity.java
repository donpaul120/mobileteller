package com.nnamdi.mobileteller.Products.Activity;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.Components.MTListView;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.Merchant.Utility.MerchantProductCategory;
import com.nnamdi.mobileteller.Products.Fragment.MTProductsFragment;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Products.Fragment.RequestFragment;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

public class MTProductsActivity extends AppCompatActivity implements MTFragmentCommunicationInterface, ItemsListCallBackInterface, ImageCacheInterface{

    private static String LOADING_ICON_VISIBILITY = "LOADING_ICON_VISIBILITY";

    private MTProductsFragment productsFragment;
    private RequestFragment requestFragment;
    private ImageView loadingFragmentIcon;
    private Animation rotate;
    private TextView connectionStatusView;
    private Button connectionRetryBtn;
    private ItemsListCallBackInterface productListCallBack;
    private Context context;
    private Toolbar toolbar;
    private LruCache<String, Bitmap> productImageCache;



    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_mtproducts);


            setConnectionRetryBtn();
            setConnectionStatusViews();

            if(savedInstanceState==null)
            {   //lets attach the fragments when this activity is created
                attachProductFragment();
                FragmentManager manager = getSupportFragmentManager();
                requestFragment = (RequestFragment) manager.findFragmentByTag(RequestFragment.TAG);
                if(requestFragment==null) {
                    requestFragment = new RequestFragment();
                    manager.beginTransaction()
                            .add(requestFragment, RequestFragment.TAG)
                            .commit();
                    //request for this just once.. so we call this only when saveinstance is null
                    requestForCategoryProducts();
                }
            }else{
                //just in-case there is a screen rotation in the process lets get the reference to the attached fragment
                //to enable the activity communicate with them
                if(getSupportFragmentManager().findFragmentByTag(MTProductsFragment.TAG)!=null){
                    productListCallBack = (ItemsListCallBackInterface)getSupportFragmentManager().findFragmentByTag(MTProductsFragment.TAG);
                }else{
                    Toast.makeText(getApplicationContext()," Eyyyyah its null as well",Toast.LENGTH_SHORT).show();
                }
                if(getSupportFragmentManager().findFragmentByTag(RequestFragment.TAG)!=null){
                    requestFragment = (RequestFragment) getSupportFragmentManager().findFragmentByTag(RequestFragment.TAG);
                }

            }
        initProductImageCache();
        setActivityToolbar();
        context = getApplicationContext();
    }

    public void initProductImageCache(){
        if(requestFragment!=null){
            productImageCache = requestFragment.bitmapLruCache;
            if(productImageCache == null){
                int maxMemory = (int) (Runtime.getRuntime().maxMemory()/1024);
                int freeMemory = (int) (Runtime.getRuntime().freeMemory()/1024);
                //Toast.makeText(this, "MAX::"+maxMemory, Toast.LENGTH_SHORT).show();
                //Toast.makeText(this,"FREE ::"+freeMemory, Toast.LENGTH_LONG).show();
                int cacheSize = maxMemory/8;
                //Toast.makeText(this,"CACHE SIZE ::"+cacheSize, Toast.LENGTH_LONG).show();
                productImageCache = new LruCache<String, Bitmap>(cacheSize){
                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getRowBytes()/1024;
                    }
                };
            }
            requestFragment.bitmapLruCache = productImageCache;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(loadingFragmentIcon!=null){
            outState.putInt(LOADING_ICON_VISIBILITY, loadingFragmentIcon.getVisibility());
        }
    }

    @Override
    protected void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            loadingFragmentIcon.startAnimation(rotate);
            //noinspection ResourceType
            loadingFragmentIcon.setVisibility(savedInstanceState.getInt(LOADING_ICON_VISIBILITY));
        }
    }

    public void requestForCategoryProducts(){
        if(requestFragment!=null){
            Bundle data = getIntent().getExtras();
            MerchantProductCategory category = data.getParcelable("CATEGORY");

            if(category!=null) {
                requestFragment.requestProductsByCategory(data.getString(MTMerchantDataKey.MERCHANT_ID), category.getCategoryId());
                startLoadingProductsRequestAnim();
                System.out.println("Requested For Products::::: "+category.getCategoryName());
            }else{
                System.out.println("Omo this thing is null:::::");
            }
        }
    }

    public void startLoadingProductsRequestAnim()
    {
        loadingFragmentIcon.setVisibility(View.VISIBLE);
        loadingFragmentIcon.startAnimation(rotate);
    }

    public void stopLoadingProductsRequestAnim()
    {
        if(rotate!=null && loadingFragmentIcon!=null) {
            rotate.cancel();
            loadingFragmentIcon.clearAnimation();
            Animation animation = new AlphaAnimation(1,0);
            animation.setDuration(300);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            loadingFragmentIcon.setAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    loadingFragmentIcon.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            animation.start();
        }
    }


    public void setConnectionRetryBtn()
    {
        connectionRetryBtn = (Button) findViewById(R.id.conn_retry);
        connectionRetryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1, 0);
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.setDuration(500);
                connectionRetryBtn.startAnimation(animation);
                connectionStatusView.startAnimation(animation);
                connectionRetryBtn.setVisibility(View.GONE);
                connectionStatusView.setVisibility(View.GONE);
            }
        });
    }

    public void setConnectionStatusViews()
    {
        connectionStatusView = (TextView) findViewById(R.id.conn_stat);
        loadingFragmentIcon = (ImageView) findViewById(R.id.load);
        rotate = AnimationUtils.loadAnimation(this, R.anim.mt_activity_loading);
    }


    public void displayConnectionError()
    {
        //TODO: Do Custom Layout for Displaying Error
        stopLoadingProductsRequestAnim();
        Animation animation = new AlphaAnimation(0, 1);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(500);
        connectionStatusView.startAnimation(animation);
        connectionRetryBtn.startAnimation(animation);
        connectionStatusView.setVisibility(View.VISIBLE);
        connectionRetryBtn.setVisibility(View.VISIBLE);
    }

    public void attachProductFragment(){
        FragmentManager manager = getSupportFragmentManager();
        productsFragment = (MTProductsFragment) manager.findFragmentByTag(MTProductsFragment.TAG);
        if(productsFragment ==null) {
            productsFragment = MTProductsFragment.newInstance();
            //a way to communicate with the fragment
            productListCallBack = (ItemsListCallBackInterface) productsFragment;

            manager.beginTransaction()
                    .add(R.id.container, productsFragment, MTProductsFragment.TAG)
                    .commit();
        }
    }

    public void setActivityToolbar()
    {
        toolbar = MTGeneralActionBar.setGeneralActionBar(this, "MobileTeller", "Products");
        if(toolbar!=null)
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mtproducts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void triggerAction(int ActionType, int id, Bundle data) {
        if(ActionType== com.nnamdi.mobileteller.Constants.ActionType.LIST_SCROLL){
            MTGeneralActionBar.animateToolbarOnListScroll(data.getInt(MTListView.SCROLL_POSITION), toolbar, this);
        }
    }

    @Override
    public void onConnectionFailed() {
        System.out.println("connection failed");
        if(productListCallBack!=null){
            productListCallBack.onConnectionFailed();
        }
        displayConnectionError();
    }

    @Override
    public void onConnectionSuccessful(final ArrayList<?> products) {
        stopLoadingProductsRequestAnim();
        if(productListCallBack!=null) {
            productListCallBack.onConnectionSuccessful(products);
        }else{
            Log.e("PRODUCT_ACTIVITY", "ProductListCallBack is NULL!!!");
        }
    }

    @Override
    public void addToCache(String urlKey, Bitmap image) {
        if(productImageCache!=null){
            if(productImageCache.get(urlKey)==null) {
                productImageCache.put(urlKey, image);
            }
        }
    }

    @Override
    public Bitmap getImageFromCache(String urlKey) {

        return productImageCache.get(urlKey);
    }
}