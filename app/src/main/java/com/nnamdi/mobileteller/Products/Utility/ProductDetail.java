package com.nnamdi.mobileteller.Products.Utility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by paulex10 on 14/08/2015.
 */
public class ProductDetail {

    private String detailName;//group name
    private String groupLabelName;
    private int groupIcon;
    private int fontSize;
    private int fontColor;
    private boolean hasLabel;
    private int labelColor;
    private int marginBottom;
    private boolean hasVariant = false;
    private HashMap<String, ProductVariant> variants;
//    private ArrayList<ProductVariant> groupNodes = new ArrayList<>();
    public ArrayList<String> groupChildren = new ArrayList<>();


    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public String getGroupLabelName() {
        return groupLabelName;
    }

    public void setGroupLabelName(String groupLabelName) {
        this.groupLabelName = groupLabelName;
    }

    public int getGroupIcon() {
        return groupIcon;
    }

    public void setGroupIcon(int groupIcon) {
        this.groupIcon = groupIcon;
    }

//    public ArrayList<ProductVariant> getGroupNodes() {
//        return groupNodes;
//    }
//
//    public void setGroupNodes(ArrayList<ProductVariant> groupNodes) {
//        this.groupNodes = groupNodes;
//    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getFontColor() {
        return fontColor;
    }

    public void setFontColor(int fontColor) {
        this.fontColor = fontColor;
    }

    public int getMarginBottom() {
        return marginBottom;
    }

    public void setMarginBottom(int marginBottom) {
        this.marginBottom = marginBottom;
    }

    public boolean isHasLabel() {
        return hasLabel;
    }

    public void setHasLabel(boolean hasLabel) {
        this.hasLabel = hasLabel;
    }

    public int getLabelColor() {
        return labelColor;
    }

    public void setLabelColor(int labelColor) {
        this.labelColor = labelColor;
    }

    public boolean isHasVariant() {
        return hasVariant;
    }

    public void setHasVariant(boolean hasVariant) {
        this.hasVariant = hasVariant;
    }

    public HashMap<String, ProductVariant> getVariants() {
        return variants;
    }

    public void setVariants(HashMap<String, ProductVariant> variants) {
        this.variants = variants;
    }
}
