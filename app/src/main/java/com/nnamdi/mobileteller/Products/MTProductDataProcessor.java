package com.nnamdi.mobileteller.Products;

import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.Products.MTProductDataKey;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * @since 2014
 * 1:20AM
 * @author Paul Okeke
 * Created by paulex10 on 21/12/2014.
 */
public class MTProductDataProcessor {


    public ArrayList<MTProduct> processProductData(JSONArray productDetails) throws JSONException
    {
        ArrayList<MTProduct> productsList = new ArrayList<MTProduct>();
        if(productDetails.length()!=0)
        {
            for (int i=0;i<productDetails.length();i++)
            {
                retrieveProductDetails(productDetails.getJSONObject(i), productsList);
            }
        }
        return productsList;
    }


    public void retrieveProductDetails(JSONObject productObject, ArrayList<MTProduct> productsList) throws JSONException
    {

        MTProduct product = new MTProduct();
        product.setProductID(productObject.getString(MTProductDataKey.PRODUCT_ID));
        product.setProductName(productObject.getString(MTProductDataKey.PRODUCT_NAME));
        product.setProductPrice(productObject.getInt(MTProductDataKey.COST_PRICE));
        product.setProductDescription(productObject.getString(MTProductDataKey.DESCRIPTION));
        product.setProductImages(productObject.getJSONArray(MTProductDataKey.PICTURE_DETAILS).getJSONObject(0).getString(MTProductDataKey.IMAGE_URL));
        product.setProductCategoryName(productObject.getJSONArray(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES).getJSONObject(0).getString(MTMerchantDataKey.CATEGORY_NAME));
        product.setProductVariantsJson(productObject.getJSONArray(MTProductDataKey.PRODUCT_VARIANTS).toString());
//        System.out.println(productObject.getString(MTProductDataKey.IMAGE_URL));
        /*product.setProductManufacturer(productObject.getString(MTProductDataKey.MANUFACTURER));
        product.setProductTimeToPrepare(productObject.getInt(MTProductDataKey.TIME_TO_PREPARE));
        product.setProductPrice(productObject.getInt(MTProductDataKey.COST_PRICE));
        product.setProductSalesPrice(productObject.getInt(MTProductDataKey.SALES_PRICE));
        product.setProductFreeShipping(productObject.getInt(MTProductDataKey.FREE_SHIPPING));
        product.setProductTaxable(productObject.getInt(MTProductDataKey.TAXABLE));
        product.setProductImages(productObject.getString(MTProductDataKey.IMAGES));
        product.setProductCreateDate(productObject.getLong(MTProductDataKey.CREATED_DATE));
        //to be continued
*/
        productsList.add(product);
    }
}
