package com.nnamdi.mobileteller.Products.Interface;

import android.os.Bundle;

import com.nnamdi.mobileteller.Products.Utility.MTProduct;

import java.util.ArrayList;

/**
 * Created by paulex10 on 04/12/2014.
 */
public interface ItemsListCallBackInterface {

    //This will hold the product list

    void onConnectionFailed();

    void onConnectionSuccessful(ArrayList<?> items);
}
