package com.nnamdi.mobileteller.Products.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.Components.MTImageHelper;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.ConnectionPool.MTImageLoader;
import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * 2:24PM
 * Created by paulex10 on 21/12/2014.
 */
public class MTProductListAdapter extends BaseAdapter {

    private ArrayList<MTProduct> products;
    private Context context;
    private ImageCacheInterface cacheInterface;

    public MTProductListAdapter(Context context , ArrayList<MTProduct> products, ImageCacheInterface cacheInterface) {
        this.products = products;
        this.context = context;
        this.cacheInterface = cacheInterface;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView==null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.product_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.productName = (TextView) convertView.findViewById(R.id.mt_product_name);
            viewHolder.productDesc = (TextView) convertView.findViewById(R.id.mt_product_desc);
            viewHolder.productPrice = (TextView) convertView.findViewById(R.id.mt_product_price);
            viewHolder.productImage = (ImageView) convertView.findViewById(R.id.merchant_main);
            viewHolder.loadingIcon = (ImageView) convertView.findViewById(R.id.image_loading_icon);
            viewHolder.merchantLogo = (ImageView) convertView.findViewById(R.id.merchant_logo);
            viewHolder.productCategoryName = (TextView) convertView.findViewById(R.id.mt_product_category_name);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        setItemValue(viewHolder,position);

        String []url =  {products.get(position).getProductImages()};
        Bitmap image = cacheInterface.getImageFromCache(url[0]);
        if(image==null) {
            viewHolder.productImage.setImageBitmap(preDetermineImageSize());
            new MTImageLoader(context, viewHolder.productImage, viewHolder.loadingIcon, cacheInterface).execute(url);
            System.out.println("This is the product url" + products.get(position).getProductImages());
        }else{
            renderDownloadedImageToView(image, viewHolder.productImage);
        }
        setItemTypeFaces(viewHolder);
        return convertView;
    }

    public void setItemValue(ViewHolder viewHolder, int itemPosition){
        viewHolder.productName.setText(products.get(itemPosition).getProductName());
        viewHolder.productDesc.setText(products.get(itemPosition).getProductDescription());
        viewHolder.productPrice.setText("$"+String.valueOf(products.get(itemPosition).getProductPrice()));
        viewHolder.productCategoryName.setText(products.get(itemPosition).getProductCategoryName());
        viewHolder.merchantLogo.setImageBitmap(MTImageHelper.createRoundBitmap(((BitmapDrawable)viewHolder.merchantLogo.getDrawable()).getBitmap()));
    }

    private void setItemTypeFaces(ViewHolder viewHolder){
        View views[] = {viewHolder.productName, viewHolder.productPrice};
        FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT_BOLD);
        View views2[] = {viewHolder.productDesc, viewHolder.productCategoryName};
        FontStyleFactory.setFontForAllViews(context, views2,StyleConstants.GENERAL_APP_FONT);
    }

    public Bitmap preDetermineImageSize(){
        Bitmap bitmap = Bitmap.createBitmap(1067,867, Bitmap.Config.ARGB_8888);
        bitmap = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
        Canvas canvas = new Canvas();
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        canvas.drawBitmap(bitmap,bitmap.getWidth(),bitmap.getHeight(),paint);
        //canvas.
        return bitmap;
    }

    private void renderDownloadedImageToView(Bitmap bitmap, final ImageView image){
        //bitmap = MTImageHelper.createRequiredBitmap(bitmap);//convert the image to a
        image.setVisibility(View.VISIBLE);
        Bitmap screenImage = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
        image.setImageBitmap(screenImage);
    }

    static class ViewHolder
    {
        TextView productName;
        TextView productDesc;
        TextView productPrice;
        ImageView productImage;
        ImageView merchantLogo;
        TextView productCategoryName;
        ImageView loadingIcon;
    }
}