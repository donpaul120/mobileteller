package com.nnamdi.mobileteller.Products.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Products.Utility.ProductDetail;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author paulex10 Okeke
 * Created by paulex10 on 28/06/2015.
 */
public class ProductDetailsAdapter extends BaseExpandableListAdapter{

    private Context context;
    private ArrayList<ProductDetail> listItems;

    public ProductDetailsAdapter(Context context, ArrayList<ProductDetail> listItems){
        this.context = context;
        this.listItems = listItems;
    }

    @Override
    public int getGroupCount() {
        return listItems.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return listItems.get(i).groupChildren.size();
    }

    @Override
    public Object getGroup(int i) {
        return listItems.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return listItems.get(i).groupChildren.get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return listItems.indexOf(listItems.get(i));
    }

    @Override
    public long getChildId(int i, int i1) {
        return listItems.get(i).groupChildren.indexOf(listItems.get(i).groupChildren.get(i1));
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        ViewHolderVariants viewHolderVariants = null;
        View convertView = view;
        int viewType = getGroupType(i);
        if(convertView==null){
            if(viewType==0) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.product_details_list_item, viewGroup, false);
                initializeGroupTypeOne(viewHolder, convertView);
            }else{
                viewHolderVariants = new ViewHolderVariants();
                convertView = LayoutInflater.from(context).inflate(R.layout.variants_child_item, viewGroup, false);
                initializeGroupTypeTwo(viewHolderVariants, convertView);
            }
        }else{
            if(viewType==0){
                viewHolder = (ViewHolder) convertView.getTag();
            }else{
                viewHolderVariants = (ViewHolderVariants) convertView.getTag();
            }
        }
        if(viewType==0){
            setFieldsForViewOne(viewHolder, i);
        }else{
            setFontsForViewTwo(viewHolderVariants, i);
        }
//        if(listItems.get(i).getGroupIcon()==0){
//            viewHolder.icon.setVisibility(View.GONE);
//        }
        return convertView;
    }

    public void setFieldsForViewOne(ViewHolder viewOne, int i){
        viewOne.productDetailName.setText(listItems.get(i).getDetailName());
        viewOne.label.setText(listItems.get(i).getGroupLabelName());
        int visibility = listItems.get(i).isHasLabel() ? View.VISIBLE : View.GONE;
        viewOne.label.setVisibility(visibility);
        viewOne.productDetailName.setTextSize(listItems.get(i).getFontSize());
    }

    public void setFontsForViewTwo(ViewHolderVariants viewTwo, int i){

    }

    public void initializeGroupTypeOne(ViewHolder viewHolder, View convertView){
        viewHolder.productDetailName = (TextView) convertView.findViewById(R.id.text_content);
        viewHolder.label = (TextView) convertView.findViewById(R.id.text_content_label);
        viewHolder.icon = (ImageView) convertView.findViewById(R.id.item_icon);
        View views[] = {viewHolder.label, viewHolder.productDetailName};
        FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);
        convertView.setTag(viewHolder);
    }

    public void initializeGroupTypeTwo(ViewHolderVariants ViewHolderVariants, View convertView){

    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        ViewHolderVariants viewHolder;
        View convertView = view;
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    @Override
    public int getGroupTypeCount() {
        return 2;
    }

    @Override
    public int getGroupType(int groupPosition) {
        if(!listItems.get(groupPosition).isHasVariant()){
            return 0;
        }else{
            return 1;
        }
    }

    static class ViewHolder{
        TextView productDetailName;
        TextView label;
        ImageView icon;
    }

    static class ViewHolderVariants {
        TextView subCategoryName;
        TextView sellingPrice;
        TextView sellingPriceLabel;
        TextView costPrice;
        TextView costPriceLabel;
        TextView label;
        ImageView icon;
    }
}
