package com.nnamdi.mobileteller.Products;

/**
 * @author Paul Okeke
 * Created by paulex10 on 21/12/2014.
 */
public class MTProductDataKey {

    //OBJECT KEY
    public static String PRODUCT="PRODUCT";

    //LIST OF PRODUCT
    public static String PRODUCTS = "PRODUCTS";

    //JSON KEYS
    public static String PRODUCT_ID = "id";
    public static String PRODUCT_NAME = "product_name";
    public static String MANUFACTURER = "manufacturer";
    public static String TIME_TO_PREPARE = "timeToPrepare";
    public static String COST_PRICE = "cost_price";
    public static String SALES_PRICE = "sales_price";
    public static String FREE_SHIPPING = "free_shipping";
    public static String TAXABLE = "tax";
    public static String DESCRIPTION = "product_description";
    public static String IMAGE_URL = "url";
    public static String PICTURE_DETAILS = "picture_details";
    public static String PRODUCT_VARIANTS = "product_variants";
    public static String IMAGES = "product_image";
    public static String CREATED_DATE = "create_date";
    //public static String UPDATED_DATE = "updatedat";
    public static String SEOTITLE = "seotitle";
    public static String META = "meta";
    public static String SKU = "sku";
    public static String COUPON_DETAILS = "couponDetails";
    public static String PRODUCT_OPTIONS = "productOptions";
}
