package com.nnamdi.mobileteller.Products.Utility;

/**
 * @author Paul Okeke
 * Created by paulex10 on 14/08/2015.
 */
public class ProductVariant {

    private String variantDetailsId;
    private String variantId;
    private String variantCategoryName;
    private String variantSubCategory;
    private int sellingPrice;
    private String displayType;


    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getVariantSubCategory() {
        return variantSubCategory;
    }

    public void setVariantSubCategory(String variantSubCategory) {
        this.variantSubCategory = variantSubCategory;
    }

    public String getVariantCategoryName() {
        return variantCategoryName;
    }

    public void setVariantCategoryName(String variantCategoryName) {
        this.variantCategoryName = variantCategoryName;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getVariantDetailsId() {
        return variantDetailsId;
    }

    public void setVariantDetailsId(String variantDetailsId) {
        this.variantDetailsId = variantDetailsId;
    }
}
