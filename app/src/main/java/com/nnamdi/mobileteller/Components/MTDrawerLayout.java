package com.nnamdi.mobileteller.Components;



import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.nnamdi.mobileteller.Help.Activity.HelpActivity;
import com.nnamdi.mobileteller.Adapters.MTDrawerAdapter;
import com.nnamdi.mobileteller.R;


/**
 * @author Paul Okeke
 * Created by paulex10 on 18/11/2014.
 */
public class MTDrawerLayout implements AdapterView.OnItemClickListener{

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout mtDrawerLayout;
    private AppCompatActivity activity;
    private int drawerIcon;
    private Toolbar toolbar;


    public MTDrawerLayout(AppCompatActivity activity,Toolbar toolbar, int drawerID, int listID)
    {
        this.activity = activity;
        this.toolbar = toolbar;
        mtDrawerLayout = (DrawerLayout) activity.findViewById(drawerID);
        ListView drawerList  =(ListView) activity.findViewById(listID);
        String drawerItemNames[] = {"My Orders","Find a merchant", "My Account", "Options and Settings", "Help"};

        drawerList.addHeaderView(LayoutInflater.from(activity).inflate(R.layout.drawer_list_header, drawerList, false));

        drawerList.setAdapter(new MTDrawerAdapter(activity.getApplicationContext(), drawerItemNames));
        setActionBarDrawerToggle();
        mtDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerList.setOnItemClickListener(this);
    }




    public void setActionBarDrawerToggle() {

        int defaultDrawerIcon = R.drawable.ic_navigation_drawer;
        if(drawerIcon==0)
        {
            drawerIcon = defaultDrawerIcon;
        }
        actionBarDrawerToggle = new ActionBarDrawerToggle(activity, mtDrawerLayout,toolbar, R.string.nav_open, R.string.nav_close)
        {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ActionBar ac = activity.getSupportActionBar();
                if(ac!=null)
                {
                    activity.invalidateOptionsMenu();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                ActionBar ac = activity.getSupportActionBar();
                if(ac!=null)
                {
                    activity.invalidateOptionsMenu();
                }
            }
        };
        actionBarDrawerToggle.setHomeAsUpIndicator(drawerIcon);
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return actionBarDrawerToggle;
    }

//    public DrawerLayout getMtDrawerLayout() {
//        return mtDrawerLayout;
//    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //TODO : Handle Drawer Nav Item Click Here @MobileTeller
        Intent intent = new Intent(activity, HelpActivity.class);

            if(i==5)
            {
                activity.startActivity(intent);
            }
    }
}
