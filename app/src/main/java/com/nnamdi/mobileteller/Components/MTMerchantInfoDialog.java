package com.nnamdi.mobileteller.Components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nnamdi.mobileteller.Merchant.Adapter.MTMerchantDetailsListAdapter;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 21/12/2014.
 */
public class MTMerchantInfoDialog {

    private Context context;
    private MTMerchantDetailsListAdapter detailsListAdapter;
    private Bundle merchantDetails;
    private ArrayList<String> merchantDetailList;

    public MTMerchantInfoDialog(Context context, Bundle merchantDetails, View detailView, View parent)
    {
        this.context = context;
        this.merchantDetails = merchantDetails;
        PopupWindow merchantDetailWindow = new PopupWindow( detailView, parent.getWidth(),parent.getHeight(),true);
        merchantDetailWindow.setOutsideTouchable(true);
        merchantDetailWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        merchantDetailWindow.showAtLocation(detailView, Gravity.CENTER, 0, 0);
        setMerchantName(detailView);
        putMerchantDetails(merchantDetails);
        setMerchantInfoDialogList(detailView);
    }

    public void putMerchantDetails(Bundle merchantDetails)
    {
        merchantDetailList = new ArrayList<String>();
        merchantDetailList.add(merchantDetails.getString(MTMerchantDataKey.MERCHANT_PHONE_OFFICE));
        merchantDetailList.add(merchantDetails.getString(MTMerchantDataKey.MERCHANT_EMAIL));
        setDetailsListAdapter(merchantDetailList);
    }


    public void setMerchantInfoDialogList(View detailView)
    {
        ListView detailList = (ListView) detailView.findViewById(R.id.detail_list);
        detailList.setAdapter(getDetailsListAdapter());
        detailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    public  void setDetailsListAdapter(ArrayList<String> merchantDetailList)
    {
        detailsListAdapter = new MTMerchantDetailsListAdapter(context, merchantDetailList);
    }

    public MTMerchantDetailsListAdapter getDetailsListAdapter() {
        return detailsListAdapter;
    }

    public void setMerchantName(View detailView)
    {
        TextView merchantName = (TextView) detailView.findViewById(R.id.mt_pop_merchant_name);
        merchantName.setText(merchantDetails.getString(MTMerchantDataKey.MERCHANT_FULL_BISNAME));
    }
}
