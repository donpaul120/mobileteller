package com.nnamdi.mobileteller.Components.Interface;

/**
 * @author Paul Okeke
 * Created by paulex10 on 06/07/2015.
 */
public interface GeneralDialogInterface {

    void onOkay(String dialogType);

    void onCancel();

}
