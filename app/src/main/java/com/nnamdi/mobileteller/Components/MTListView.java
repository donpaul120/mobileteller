package com.nnamdi.mobileteller.Components;

import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;

import com.nnamdi.mobileteller.Constants.ActionType;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;

/**
 * @author Paul Okeke
 * Created by paulex10 on 30/06/2015.
 */
public class MTListView {

    public final static String SCROLL_POSITION = "SCROLL_POSITION";

    public static void dispatchScrollEvent(ListView listView, final MTFragmentCommunicationInterface mListener){

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            Bundle data = new Bundle();

            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if(i==0){
                    data.putInt(SCROLL_POSITION, i);
                    mListener.triggerAction(ActionType.LIST_SCROLL, 0, data);
                }else if(i>0){
                    data.putInt(SCROLL_POSITION, i);
                    mListener.triggerAction(ActionType.LIST_SCROLL, 0, data);
                }
            }
        });
    }
}
