package com.nnamdi.mobileteller.Components;


import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.R;

/**
 * @author Paul Okeke
 * Created by paulex10 on 19/11/2014.
 */
public class MTGeneralActionBar {



    @Nullable
    public static Toolbar setGeneralActionBar(AppCompatActivity activity, String appTitle, String activityTitle)
    {
       Toolbar toolbar = (Toolbar) activity.findViewById(R.id.mt_toolbar_include);
        if(toolbar!=null)
        {
            TextView toolBarTitle = (TextView) toolbar.findViewById(R.id.mt_toolbar_title);
            toolBarTitle.setText(appTitle);

            TextView toolBarSubTitle = (TextView) toolbar.findViewById(R.id.mt_toolbar_subtitle);
            toolBarSubTitle.setText(activityTitle);
            View views [] = {toolBarTitle, toolBarSubTitle};
            FontStyleFactory.setFontForAllViews(activity, views, StyleConstants.GENERAL_APP_FONT);
            return toolbar;
        }
        else
        {
          return null;
        }
    }
    static  boolean isShown = false;
    public static void animateToolbarOnListScroll(int index, Toolbar toolbar, AppCompatActivity activity){
        if (index != 0 && isShown) {
            Animation anim = new TranslateAnimation(0, 0, 0, -toolbar.getHeight());
            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(800);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);
            activity.findViewById(R.id.view).setVisibility(View.GONE);
            //activity.findViewById(R.id.view).startAnimation(anim);
            toolbar.startAnimation(anim);
            isShown = false;
        } else if (index == 0 && !isShown) {
            Animation anim = new TranslateAnimation(0, 0, toolbar.getTop(), 0);
            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            anim.setDuration(1000);
            anim.setFillAfter(true);
            anim.setFillEnabled(true);
            activity.findViewById(R.id.view).setVisibility(View.VISIBLE);
            //activity.findViewById(R.id.view).startAnimation(anim);
            toolbar.startAnimation(anim);
            isShown = true;
        }
    }

}
