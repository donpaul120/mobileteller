package com.nnamdi.mobileteller.Components;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nnamdi.mobileteller.Components.Interface.GeneralDialogInterface;
import com.nnamdi.mobileteller.Constants.AuthConstant;
import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.R;

/**
 * @author Paul Okeke
 * Created by paulex10 on 16/04/2015.
 */
public class GeneralDialog {

    private Context context;
    private View dialogView;
    private PopupWindow authPopUp;
    private GeneralDialogInterface dialogInterface;
    private String primaryBtnTitle = "";
    private String secondaryBtnTitle = "CANCEL";
    private String dialogType;

    /**
     *
     * @param context Android Context
     * @param dialogView    This is the Inflated View (@layout) of the dialog
     * @param parentView    This is the parent view of the Activity
     * @param dialogDetails A Bundle that contains information that is used to set the dialog properties
     */
    public GeneralDialog(Context context, View dialogView, View parentView, Bundle dialogDetails)
    {
        this.context = context;
        this.dialogView = dialogView;
        authPopUp = new PopupWindow(dialogView,parentView.getWidth(), parentView.getHeight(), true);
//      authPopUp.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        authPopUp.setOutsideTouchable(true);
        authPopUp.showAtLocation(dialogView, Gravity.CENTER, 100, 100);
        setDialogView(dialogDetails);
    }

    /**
     * We need to check that the bundle contains the key
     * @param dialogDetails
     */
    public void setDialogView(Bundle dialogDetails)
    {
        if(dialogDetails.containsKey(AuthConstant.AUTH_DIALOG_TITLE))
        {
            setDialogTitle(dialogDetails.getString(AuthConstant.AUTH_DIALOG_TITLE));
        }
        if(dialogDetails.containsKey(AuthConstant.AUTH_DIALOG_CONTENT))
        {
            setDialogContent(dialogDetails.getString(AuthConstant.AUTH_DIALOG_CONTENT));
        }
        //whether or not it contains a header lets set the ok button so that it closes the dialog
        setOkButtonListener();
        setCancelButtonListener();
    }

    public void setDialogTitle(String dialogTitle)
    {
        TextView title = (TextView) dialogView.findViewById(R.id.hb_auth_dialog_title);
        title.setText(dialogTitle);
        FontStyleFactory.setFontForView(context,title, StyleConstants.GENERAL_APP_FONT);
    }

    public void setDialogContent(String dialogContent)
    {
        TextView dialogInfo = (TextView) dialogView.findViewById(R.id.hb_auth_dialog_content);
        dialogInfo.setText(dialogContent);
        if(dialogContent.isEmpty()){
            dialogInfo.setVisibility(View.GONE);
        }
        FontStyleFactory.setFontForView(context,dialogInfo, StyleConstants.GENERAL_APP_FONT);
    }

    public void setOkButtonListener()
    {
        Button okBtn = (Button) dialogView.findViewById(R.id.hb_auth_dialog_ok_btn);
        FontStyleFactory.setFontForView(context,okBtn, StyleConstants.GENERAL_APP_FONT_BOLD);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialogInterface!=null) {
                    dialogInterface.onOkay(dialogType);
                }
                    authPopUp.dismiss();
            }
        });
        if(primaryBtnTitle.isEmpty() || primaryBtnTitle.length()==0){
            okBtn.setVisibility(View.GONE);
        }else {
            okBtn.setText(primaryBtnTitle);
            okBtn.setVisibility(View.VISIBLE);
        }
    }

    public void setCancelButtonListener(){
        final Button cancelBtn = (Button) dialogView.findViewById(R.id.hb_auth_dialog_cancel_btn);
        FontStyleFactory.setFontForView(context, cancelBtn, StyleConstants.GENERAL_APP_FONT_BOLD);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialogInterface!=null){
                    dialogInterface.onCancel();
                }
                authPopUp.dismiss();
            }
        });
        if(secondaryBtnTitle.isEmpty() || secondaryBtnTitle.trim().length()==0){
            //
        }else{
            cancelBtn.setText(secondaryBtnTitle);
            cancelBtn.setVisibility(View.VISIBLE);
        }
    }

    public void setPrimaryBtnTitle(String primaryBtnTitle) {
        this.primaryBtnTitle = primaryBtnTitle;
        setOkButtonListener();
    }

    public void setSecondaryBtnTitle(String secondaryBtnTitle) {
        this.secondaryBtnTitle = secondaryBtnTitle;
        setCancelButtonListener();
    }

    public void setDialogInterface(GeneralDialogInterface dialogInterface){
        this.dialogInterface = dialogInterface;
    }
    public void fadeOutDialogView(View dialogView){
        Animation animation = new AlphaAnimation(1,0);
        animation.setDuration(500);
        dialogView.startAnimation(animation);
    }

    public void setDialogType(String dialogType) {
        this.dialogType = dialogType;
    }
}