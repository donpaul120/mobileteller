package com.nnamdi.mobileteller.Components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * @author Paul Okeke
 * Created by paulex10 on 20/06/2015.
 */
public class MTImageHelper {

    public final static int REQUIRED_IMAGE_WIDTH = 1067;
    public final static int REQUIRED_IMAGE_HEIGHT = 867;

    public final static int CACHE_IMAGE_WIDTH = 400;
    public final static int CACHE_IMAGE_HEIGHT = 400;

    public static Bitmap createCachedBitmap(Bitmap image){

        int imgH = image.getHeight();
        int imgW = image.getWidth();

        float scaleFH = (float)CACHE_IMAGE_HEIGHT / imgH;
        float scaleFW = (float)CACHE_IMAGE_WIDTH / imgW;

        int newH = (int)(imgH * scaleFH);
        int newW = (int)(imgW * scaleFW);

        image = Bitmap.createScaledBitmap(image, newW, newH, false);
        return image;
    }


    /**
     * By Paul Okeke
     * @param image u
     * @return Bitmap
     */
    public static Bitmap createRequiredBitmap(Bitmap image){

        int imgH = image.getHeight();
        int imgW = image.getWidth();

        float scaleFH = (float)REQUIRED_IMAGE_HEIGHT / imgH;
        float scaleFW = (float)REQUIRED_IMAGE_WIDTH/ imgW;

        int newH = (int)(imgH * scaleFH);
        int newW = (int)(imgW * scaleFW);

        image = Bitmap.createScaledBitmap(image, newW, newH, false);
        //Log.e("PRODUCT CACHED SIZE", ""+(image.getByteCount())/1024);
        return image;
    }

    public static Bitmap createScreenFitBitmap(Context context, Bitmap image){

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        int sWidth = metrics.widthPixels;

        int imgW = image.getWidth();
        int imgH = image.getHeight();

        float scaleF = (float)sWidth/imgW;

        int newW = (int)(imgW*scaleF);
        int newH = (int)(imgH*scaleF);

        image = Bitmap.createScaledBitmap(image,newW, newH, false);
        image.setDensity(metrics.densityDpi);
        //Log.e("PRODUCT IMAGE SIZE", ""+(image.getByteCount())/1024);
        return image;
    }

    public static Bitmap createRoundBitmap(Bitmap image){
        image = Bitmap.createScaledBitmap(image, 80, 80, false);
        Bitmap layer = Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(layer);

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#FFFFFF"));
        Rect rect = new Rect(0,0, image.getWidth(), image.getHeight());
        RectF rectF = new RectF(rect);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, 6.0f, 6.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        canvas.drawBitmap(image, rect, rect, paint);
        Log.e("SIZE:: : " , ""+(layer.getRowBytes() * layer.getHeight())/1024);
        return layer;
    }

}
