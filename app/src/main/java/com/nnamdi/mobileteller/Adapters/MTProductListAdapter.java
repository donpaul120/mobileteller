package com.nnamdi.mobileteller.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nnamdi.mobileteller.POJO.Products.MTProduct;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * 2:24PM
 * Created by paulex10 on 21/12/2014.
 */
public class MTProductListAdapter extends BaseAdapter {

    private ArrayList<MTProduct> products;
    private Context context;

    public MTProductListAdapter(Context context , ArrayList<MTProduct> products) {
        this.products = products;
        this.context = context;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.product_list_item, parent, false);

        ViewHolder viewHolder = new ViewHolder();

        viewHolder.productName = (TextView) convertView.findViewById(R.id.mt_product_name);
        viewHolder.productDesc = (TextView) convertView.findViewById(R.id.mt_product_desc);

        viewHolder.productName.setText(products.get(position).getProductName());
        viewHolder.productName.setTypeface(Typeface.createFromAsset(context.getAssets(), "MobileTellerGen.otf"));
        viewHolder.productDesc.setTypeface(Typeface.createFromAsset(context.getAssets(), "MobileTellerProductDesc.ttf"));

        convertView.setTag(viewHolder);

        return convertView;
    }



    static class ViewHolder
    {
        TextView productName;
        TextView productDesc;

    }
}
