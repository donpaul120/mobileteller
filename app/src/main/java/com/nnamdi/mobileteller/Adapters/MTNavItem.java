package com.nnamdi.mobileteller.Adapters;

/**
 * @author Paul Okeke
 * Created by paulex10 on 18/11/2014.
 */
public class MTNavItem {

    private String navTitle;
    private int navIcon;

    public void setNavTitle(String navTitle) {
        this.navTitle = navTitle;
    }

    public String getNavTitle() {
        return navTitle;
    }


    public void setNavIcon(int navIcon) {
        this.navIcon = navIcon;
    }

    public int getNavIcon() {
        return navIcon;
    }
}
