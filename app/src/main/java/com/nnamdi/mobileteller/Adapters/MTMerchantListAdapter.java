package com.nnamdi.mobileteller.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.POJO.Merchants.MTMerchant;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 12/11/2014.
 */
public class MTMerchantListAdapter extends BaseAdapter{

    private ArrayList<MTMerchant> mtMerchants;
    private Context context;


    public MTMerchantListAdapter(ArrayList<MTMerchant> mtMerchants, Context context){
        this.context = context;
        this.mtMerchants = mtMerchants;
    }

    @Override
    public int getCount() {
        return mtMerchants.size();
    }

    @Override
    public Object getItem(int i) {
        return mtMerchants.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         convertView = layoutInflater.inflate(R.layout.merchant_item, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder();

            if (convertView!=null)
            {
                viewHolder.merchantLogo = (ImageView) convertView.findViewById(R.id.merchant_main);
                viewHolder.merchantBisName = (TextView) convertView.findViewById(R.id.product_name);

                MTMerchant merchant = mtMerchants.get(i);
                viewHolder.merchantLogo.setImageResource(merchant.getMerchantLogo());
                viewHolder.merchantBisName.setText(merchant.getMerchantFullBisName());
                viewHolder.merchantBisName.setTypeface(Typeface.createFromAsset(context.getAssets(), "MobileTellerGen.otf"));
                convertView.setTag(viewHolder);

                return convertView;
            }
        return null;
    }

    static class ViewHolder
    {
        //Declare our components here.
        ImageView merchantLogo;
        TextView merchantBisName;
    }
}
