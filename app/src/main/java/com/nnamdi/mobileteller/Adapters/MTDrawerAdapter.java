package com.nnamdi.mobileteller.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;


/**
 * @author Paul Okeke
 * Created by paulex10 on 18/11/2014.
 */
public class MTDrawerAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<MTNavItem> navItemArrayList;

    public MTDrawerAdapter(Context context, String[] items)
    {
        this.context = context;
        navItemArrayList = new ArrayList<MTNavItem>();
        int i =0;
            for(String item : items)
            {
                MTNavItem navItem = new MTNavItem();
                navItem.setNavTitle(item);
                if(i==0)
                    navItem.setNavIcon(R.drawable.ic_shopping_basket_black_24dp);
                if(i==1)
                    navItem.setNavIcon(R.drawable.ic_location_searching_black_24dp);
                if(i==2)
                    navItem.setNavIcon(R.drawable.ic_account_circle_black_24dp);
                if(i==3)
                    navItem.setNavIcon(R.drawable.ic_settings_black_24dp);
                if(i==4)
                    navItem.setNavIcon(R.drawable.ic_help_black_24dp);
                navItemArrayList.add(navItem);
                i++;
            }
        //navItemArrayList.add(0,null);
    }

    @Override
    public int getCount() {
        return navItemArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return navItemArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder;

        if(view==null)
        {
            view = LayoutInflater.from(context).inflate(R.layout.mobile_teller_nav_item_list, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.navIcon = (ImageView) view.findViewById(R.id.mt_nav_icon);
            viewHolder.navTitle = (TextView) view.findViewById(R.id.mt_nav_title);
            viewHolder.navItemCount = (TextView) view.findViewById(R.id.nav_item_count);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
            viewHolder.navTitle.setText(navItemArrayList.get(i).getNavTitle());
            viewHolder.navIcon.setImageResource(navItemArrayList.get(i).getNavIcon());
            viewHolder.navIcon.setColorFilter(Color.parseColor("#757575"));
            View views[] = {viewHolder.navTitle, viewHolder.navItemCount};
            FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);
        if(i==0){
            viewHolder.navIcon.setColorFilter(Color.parseColor("#FF9100"));
            viewHolder.navItemCount.setVisibility(View.VISIBLE);
        }if(i==1){
            viewHolder.navIcon.setColorFilter(Color.parseColor("#448AFF"));
        }
        return view;
    }

    static class ViewHolder
    {
        ImageView navIcon;
        TextView navTitle;
        TextView navItemCount;
    }
}