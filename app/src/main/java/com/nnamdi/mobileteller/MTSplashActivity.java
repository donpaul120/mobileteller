package com.nnamdi.mobileteller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.nnamdi.mobileteller.Merchant.Activity.MTMerchantActivity;
import com.nnamdi.mobileteller.R;

import org.json.JSONArray;


public class MTSplashActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        createCartJson();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               Intent intent = new Intent(getApplicationContext(), MTMerchantActivity.class);
                startActivity(intent);
            }
        },4000);

    }

    public void createCartJson(){
        HBSessionManager sessionManager = new HBSessionManager(this);
        if(!sessionManager.getSharedPref().contains("CART")) {
            JSONArray prodCartArgs = new JSONArray();
            sessionManager.put("CART", prodCartArgs.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
