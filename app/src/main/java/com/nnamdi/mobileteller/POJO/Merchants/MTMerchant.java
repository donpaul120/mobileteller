package com.nnamdi.mobileteller.POJO.Merchants;

import android.os.Parcel;
import android.os.Parcelable;

import com.nnamdi.mobileteller.POJO.Products.MTProduct;

import java.util.ArrayList;

/**
 *
 * Created by paulex10 on 20/12/2014.
 */
public class MTMerchant implements Parcelable{

    private String merchantID;
    private String merchantEmail;
    private String merchantFullBisName;
    private String merchantShortBisName;
    private String merchantRcNumber;
    private int merchantLogo;
    private String merchantFacebookHandle;
    private String merchantTwitterHandle;
    private int accountStatus;
    private String merchantWebsite;
    private String merchantApiId;
    private String merchantPhoneOffice;
    private String merchantPhoneAlternate;
    private ArrayList<MTProduct> merchantProductDetails = new ArrayList<MTProduct>();



    public MTMerchant()
    {
       merchantProductDetails = new ArrayList<MTProduct>();
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantFullBisName(String merchantFullBisName) {
        this.merchantFullBisName = merchantFullBisName;
    }

    public String getMerchantFullBisName() {
        return merchantFullBisName;
    }

    public void setMerchantShortBisName(String merchantShortBisName) {
        this.merchantShortBisName = merchantShortBisName;
    }

    public String getMerchantShortBisName() {
        return merchantShortBisName;
    }

    public void setMerchantRcNumber(String merchantRcNumber) {
        this.merchantRcNumber = merchantRcNumber;
    }

    public String getMerchantRcNumber() {
        return merchantRcNumber;
    }

    public void setMerchantLogo(int merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    public int getMerchantLogo() {
        return merchantLogo;
    }

    public void setMerchantFacebookHandle(String merchantFacebookHandle) {
        this.merchantFacebookHandle = merchantFacebookHandle;
    }

    public String getMerchantFacebookHandle() {
        return merchantFacebookHandle;
    }

    public void setMerchantTwitterHandle(String merchantTwitterHandle) {
        this.merchantTwitterHandle = merchantTwitterHandle;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public String getMerchantTwitterHandle() {
        return merchantTwitterHandle;
    }

    public void setMerchantWebsite(String merchantWebsite) {
        this.merchantWebsite = merchantWebsite;
    }

    public String getMerchantWebsite() {
        return merchantWebsite;
    }

    public void setMerchantApiId(String merchantApiId) {
        this.merchantApiId = merchantApiId;
    }

    public String getMerchantApiId() {
        return merchantApiId;
    }

    public void setMerchantPhoneOffice(String merchantPhoneOffice) {
        this.merchantPhoneOffice = merchantPhoneOffice;
    }

    public String getMerchantPhoneOffice() {
        return merchantPhoneOffice;
    }

    public void setMerchantPhoneAlternate(String merchantPhoneAlternate) {
        this.merchantPhoneAlternate = merchantPhoneAlternate;
    }

    public String getMerchantPhoneAlternate() {
        return merchantPhoneAlternate;
    }

    public void setMerchantProductDetails(ArrayList<MTProduct> merchantProductDetails) {
        this.merchantProductDetails = merchantProductDetails;
    }

    public ArrayList<MTProduct> getMerchantProductDetails() {
        return merchantProductDetails;
    }

    //@SuppressWarnings(Unchecked)
    public MTMerchant(Parcel parcel)
    {


        this.merchantID = parcel.readString();
        this.merchantEmail = parcel.readString();
        this.merchantFullBisName = parcel.readString();
        this.merchantShortBisName = parcel.readString();
        this.merchantRcNumber = parcel.readString();
        this.merchantLogo = parcel.readInt();
        this.merchantFacebookHandle = parcel.readString();
        this.merchantTwitterHandle = parcel.readString();
        this.accountStatus = parcel.readInt();
        this.merchantWebsite = parcel.readString();
        this.merchantApiId = parcel.readString();
        this.merchantPhoneOffice = parcel.readString();
        this.merchantPhoneAlternate = parcel.readString();
        parcel.readTypedList(merchantProductDetails, MTProduct.CREATOR);
    }

    public static Creator<MTMerchant> CREATOR = new Creator<MTMerchant>() {
        @Override
        public MTMerchant createFromParcel(Parcel source) {
            return new MTMerchant(source);
        }

        @Override
        public MTMerchant[] newArray(int size) {
            return new MTMerchant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(merchantID);
        destination.writeString(merchantEmail);
        destination.writeString(merchantFullBisName);
        destination.writeString(merchantShortBisName);
        destination.writeString(merchantRcNumber);
        destination.writeInt(merchantLogo);
        destination.writeString(merchantFacebookHandle);
        destination.writeString(merchantTwitterHandle);
        destination.writeInt(accountStatus);
        destination.writeString(merchantApiId);
        destination.writeString(merchantPhoneOffice);
        destination.writeString(merchantPhoneAlternate);
        destination.writeTypedList(merchantProductDetails);
    }
}
