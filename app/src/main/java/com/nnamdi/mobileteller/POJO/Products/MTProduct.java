package com.nnamdi.mobileteller.POJO.Products;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author Paul Okeke
 * Created by paulex10 on 12/11/2014.
 */
public class MTProduct implements Parcelable{

    private String productID;
    private String productName;
    private String productManufacturer;
    private int productTimeToPrepare;
    private int productPrice;
    private int productSalesPrice;
    private int productFreeShipping;
    private int productTaxable;
    private String productDescription;
    private String productImages;
    private Date productCreateDate;
    private Date productUpdateDate;
    private String productSeoTitle;
    private String productMeta;
    private String productSku;
    private ArrayList<String> productCouponDetails;
    private ArrayList<String> productOptions;




    /**
     *
     * Default Constructor for Product
     */
    public MTProduct()
    {}

    public void setProductID(String productID)
    {
        this.productID = productID;
    }

    public String getProductID()
    {
        return productID;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductTimeToPrepare(int productTimeToPrepare) {
        this.productTimeToPrepare = productTimeToPrepare;
    }

    public int getProductTimeToPrepare() {
        return productTimeToPrepare;
    }

    public void setProductPrice(int productPrice){
        this.productPrice = productPrice;
    }

    public double getProductPrice(){
        return productPrice;
    }

    public void setProductSalesPrice(int productSalesPrice) {
        this.productSalesPrice = productSalesPrice;
    }

    public int getProductSalesPrice() {
        return productSalesPrice;
    }

    public void setProductFreeShipping(int productFreeShipping) {
        this.productFreeShipping = productFreeShipping;
    }

    public int getProductFreeShipping() {
        return productFreeShipping;
    }

    public void setProductTaxable(int productTaxable) {
        this.productTaxable = productTaxable;
    }

    public int getProductTaxable() {
        return productTaxable;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductImages(String productImages) {
        this.productImages = productImages;
    }

    public String getProductImages() {
        return productImages;
    }

    public void setProductCreateDate(long productCreateDate) {
        this.productCreateDate = new Date(productCreateDate);
        System.out.println("DATESSDDSD :::: " + this.productCreateDate.toString());
    }

    public Date getProductCreateDate() {
        return productCreateDate;
    }

    public void setProductUpdateDate(long productUpdateDate) {
        this.productUpdateDate = new Date(productUpdateDate);
    }

    public Date getProductUpdateDate() {
        return productUpdateDate;
    }

    public void setProductSeoTitle(String productSeoTitle) {
        this.productSeoTitle = productSeoTitle;
    }

    public String getProductSeoTitle() {
        return productSeoTitle;
    }

    public void setProductMeta(String productMeta) {
        this.productMeta = productMeta;
    }

    public String getProductMeta() {
        return productMeta;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductCouponDetails(ArrayList<String> productCouponDetails) {
        this.productCouponDetails = productCouponDetails;
    }

    public ArrayList<String> getProductCouponDetails() {
        return productCouponDetails;
    }

    public void setProductOptions(ArrayList<String> productOptions) {
        this.productOptions = productOptions;
    }

    public ArrayList<String> getProductOptions() {
        return productOptions;
    }

    /**
     * This constructor is called by the Creator Class
     * @param parcel    parcel what makes up the product part
     *
    */
    private MTProduct(Parcel parcel)
    {
        this.productID = parcel.readString();
        this.productName = parcel.readString();
        this.productPrice = parcel.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(productID);
        destination.writeString(productName);
        destination.writeInt(productPrice);
    }

    public static Creator<MTProduct> CREATOR = new Creator<MTProduct>() {
        @Override
        public MTProduct createFromParcel(Parcel source) {

            return new MTProduct(source);
        }

        @Override
        public MTProduct[] newArray(int size) {
            return new MTProduct[size];
        }
    };
}
