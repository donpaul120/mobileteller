package com.nnamdi.mobileteller.Constants;

/**
 * @author Paul Okeke
 * Created by paulex10 on 07/12/2014.
 */
public class MTRequestPath {

    public static String MT_BASE_URL_SCHEME="http";
    public static String MT_BASE_URL_HOST="64.90.181.100:9000/api";

    public static String MT_MERCHANT_LIST = "merchants/all";

    public static String MT_MERCHANT_CAT_PRODUCTS = "category/products";
    public static String MT_MERCHANT_CAT_PRODUCTS_COUNT = "products/category/?/count";

    public static String MT_PRODUCT_LIST = "products/list";

    public static String MT_MERCHANT_PRODUCTS = "products/list/?/products";

}
