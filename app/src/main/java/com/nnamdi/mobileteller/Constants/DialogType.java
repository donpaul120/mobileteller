package com.nnamdi.mobileteller.Constants;

/**
 * Created by paulex10 on 12/07/2015.
 */
public class DialogType {

    public final static String NORMAL = "NORMAL";
    public final static String ADD_TO_CART = "ADD_TO_CART";
    public final static String REMOVE_FROM_CART = "REMOVE_FROM_CART";


}
