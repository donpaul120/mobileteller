package com.nnamdi.mobileteller.Constants;

/**
 * @author Paul Okeke
 * Created by paulex10 on 30/06/2015.
 */
public class ActionType {

    public static int LIST_SCROLL = 0x100;
    public static int LIST_DATA_REFRESH = 0x200;

}
