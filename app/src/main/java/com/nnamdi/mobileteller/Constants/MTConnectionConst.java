package com.nnamdi.mobileteller.Constants;

/**
 * MobileTeller
 * Created by paulex10 on 05/12/2014.
 */
public class MTConnectionConst {

    public static String REQUEST_RESPONSE="REQUEST_RESPONSE";

    public static String CONNECTION_TIMEOUT = "CONNECTION_TIMEOUT";


    public static String CONNECTION_STATUS_KEY = "CONNECT_STATUS";


    //connection message
    public static String CONNECTION_SUCCESS = "SUCCESS";
    public static String CONNECTION_FAILED = "FAILED";
    public static String CONNECTION_REFUSED = "check your internet connection";
    //public static String
}
