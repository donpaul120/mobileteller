package com.nnamdi.mobileteller.Constants;

/**
 * @author Paul Okeke
 * Created by paulex10 on 16/04/2015.
 */
public class AuthConstant {


    public static String AUTH_DIALOG_TITLE = "DIALOG_TITLE";
    public static String AUTH_DIALOG_CONTENT = "DIALOG_CONTENT";



    //Profile Alert add
    public static String ADD_TO_CART = "Add to cart";
    public static String ADD_TO_CART_MESSAGE = "Add this product to cart?";

    public static String REMOVE_FROM_CART = "Remove Product from cart";
    public static String REMOVE_FROM_CART_MESSAGE = "This product will be removed from your cart";

    public static String getConnectionFailedMessage(String messageConst, String appendMessage)
    {
        return messageConst + " " + appendMessage;
    }
}
