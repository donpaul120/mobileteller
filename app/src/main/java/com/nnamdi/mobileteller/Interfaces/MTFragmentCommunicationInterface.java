package com.nnamdi.mobileteller.Interfaces;

import android.os.Bundle;

/**
 * @author Paul Okeke
 * Created by paulex10 on 21/11/2014.
 * 8:05PM at DMATEL Hotel
 */
public interface MTFragmentCommunicationInterface {
    /**
     * By MobileTeller
     * The Implementing class uses this method
     * To verify what type of Action it is to handle
     * @param ActionType This parameter specifies the ActionType
     *                   An actionType value corresponds to already
     *                   defined static variables in the
     *                   com.nnamdi.buisness.Constant.Actions
     **/
    public void triggerAction(String ActionType, int id, Bundle data);
}
