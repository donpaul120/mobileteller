package com.nnamdi.mobileteller.Interfaces;

import android.os.Bundle;

/**
 * @author Paul Okeke
 * Created by paulex10 on 12/11/2014.
 * 19:24
 */
public interface MTMerchantCallBackInterface {

    public void getData(Bundle data);
}
