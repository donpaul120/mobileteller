package com.nnamdi.mobileteller.ConnectionPool.Interfaces;

import android.graphics.Bitmap;

/**
 * Created by paulex10 on 07/07/2015.
 */
public interface ImageCacheInterface {

    void addToCache(String urlKey, Bitmap image);

    Bitmap getImageFromCache(String urlKey);
}
