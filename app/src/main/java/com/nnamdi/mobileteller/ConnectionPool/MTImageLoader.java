package com.nnamdi.mobileteller.ConnectionPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Components.MTImageHelper;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.R;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * @author  Paul Okeke
 * Created by paulex10 on 16/06/2015.
 */
public class MTImageLoader extends AsyncTask<String , Integer, Bitmap> {

    private final WeakReference<ImageView> imageRef;
    private final WeakReference<ImageView> loadingIcon;
    private Bitmap bitmap;
    private Animation rotate;
    private Context context;
    private static int STATUS=1;
    private ImageCacheInterface imageCache;
    private Animation animation;
    private Animation.AnimationListener listener;
    private String url;

    public MTImageLoader(Context context, ImageView imageView, ImageView loadingIcon, ImageCacheInterface cacheInterface){
        imageRef = new WeakReference<ImageView>(imageView);
        this.loadingIcon = new WeakReference<ImageView>(loadingIcon);
        this.context = context;
        this.imageCache = cacheInterface;
        animation = new AlphaAnimation(0,1);

    }

    public static void loadImage(){

    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String url = params[0];
        this.url = url;
        Bitmap imageBitmap = imageCache.getImageFromCache(url);
        if(imageBitmap==null) {
            downloadImage(url);
        }else{
            bitmap = imageBitmap;
        }
        return bitmap;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ImageView loading = loadingIcon.get();
        if(loading!=null) {
            loading.setVisibility(View.VISIBLE);
            rotate = AnimationUtils.loadAnimation(context, R.anim.mt_activity_loading);
            loading.startAnimation(rotate);
        }
        System.out.println("PRE-EXECUTING OOOO");
    }

    public void downloadImage(String url){
        try{
            HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null,null, new java.security.SecureRandom());
            connection.setSSLSocketFactory(context.getSocketFactory());
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream input = null;
                try {
                    input = connection.getInputStream();
                    bitmap = BitmapFactory.decodeStream(input);
                    STATUS = 1;
                } finally {
                    if (input != null)
                        input.close();
                    connection.disconnect();
                }
                STATUS =1;
                System.out.println("Response was successful");
            }else{
                STATUS =0;
                System.out.print("Response wasn't successful oooo");
                System.out.println("ERROR CODE ::" + connection.getResponseCode() + " ---- " + connection.getResponseMessage());
            }
        }catch (IOException ex){
            ex.printStackTrace();
            System.err.println(ex.getMessage());
            STATUS = 0;
        }catch (NoSuchAlgorithmException ex){
            ex.printStackTrace();
        }catch (KeyManagementException kg){
            kg.printStackTrace();
        }
    }

    public boolean onError(){
        return (STATUS==0);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        ImageView loading = loadingIcon.get();
        if(loading!=null) {
            rotate.cancel();
            loading.clearAnimation();
            loading.setVisibility(View.GONE);
        }
        if(onError()){
            imageRef.get().setImageResource(R.drawable.image_loader_background);
            System.out.println("An Error Occured while downloading image");
            //this.execute();
        }else{
            final ImageView image = imageRef.get();
            try {
                bitmap = MTImageHelper.createRequiredBitmap(bitmap);//convert the image to a
                imageCache.addToCache(url, bitmap);
                renderDownloadedImageToView(bitmap, image);
            }catch(ClassCastException ex){
                System.err.println(ex.getMessage());
            }
            Log.i("IMAGE_SET", "Image has being set to this bitmap");
        }
    }

    private void renderDownloadedImageToView(Bitmap bitmap, final ImageView image){
        image.setVisibility(View.INVISIBLE);
        Bitmap screenImage = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
        image.setImageBitmap(screenImage);
        animation.setDuration(1300);
        image.setAnimation(animation);
        animation.start();
        image.setVisibility(View.VISIBLE);
    }
}