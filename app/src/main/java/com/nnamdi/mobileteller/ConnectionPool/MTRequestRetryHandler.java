package com.nnamdi.mobileteller.ConnectionPool;

import android.os.Bundle;

import com.nnamdi.mobileteller.Constants.MTConnectionConst;

import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Company MobileTeller
 * @since 2014
 * 3:32AM Bosun's House
 * Created by paulex10 on 06/12/2014.
 */
public class MTRequestRetryHandler implements HttpRequestRetryHandler {

    public Bundle dataBundle;

    public MTRequestRetryHandler(Bundle dataBundle)
    {
        this.dataBundle = dataBundle;
    }

    /**
     * MobileTeller.com
     * @since 2014
     * 6th-12-2014
     * 4:00AM
     *
     * @param e     Exception Thrown
     * @param i     Retry Count
     * @param httpContext   HttpContext
     * @return      Boolean
     */
    @Override
    public boolean retryRequest(IOException e, int i, HttpContext httpContext) {
        if(i>=2)
        {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            System.out.println("COUNT ::: " + "I have Exceeded"+i + " REtry COUNT");
            return false;

        }
        if(e instanceof UnknownHostException)
        {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            return false;
        }
        if(e instanceof InterruptedIOException)
        {
            //connection Timed Out
            System.out.println("This is a connection Time out shitttttttttttttTTTT");
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            return false;
        }
        if(e instanceof ConnectException)
        {
            System.out.println("This is a connection Refused shitttttttttttttTTTT");
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_REFUSED);
            return false;
        }
        if(e instanceof SocketException)
        {
            System.out.println("This is a connection Timeout Socket shitttttttttttttTTTT");
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            return false;
        }
        if(e instanceof SocketTimeoutException)
        {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            return false;
        }

        return false;
    }
}
