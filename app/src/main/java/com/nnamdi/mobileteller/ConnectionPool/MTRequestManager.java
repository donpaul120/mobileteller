package com.nnamdi.mobileteller.ConnectionPool;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.nnamdi.mobileteller.Constants.Actions;
import com.nnamdi.mobileteller.Constants.MTConnectionConst;
import com.nnamdi.mobileteller.Constants.MTRequestPath;
import com.nnamdi.mobileteller.Merchant.Interface.MTMerchantCallBackInterface;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataProcessor;
import com.nnamdi.mobileteller.Merchant.Utility.MTMerchant;
import com.nnamdi.mobileteller.Merchant.Utility.MerchantProductCategory;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONException;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 04/12/2014.
 */
public class MTRequestManager extends AsyncTask<String, Integer, Bundle> {

    /**
     *
     */
    private MTMerchantCallBackInterface callingActivity;
    private Bundle dataBundle;
    private String actionType;
    private String requestUrl;

    private ItemsListCallBackInterface listInterface;
    private ArrayList<MTProduct> products;
    private ArrayList<MTMerchant> merchants;
    private ArrayList<MerchantProductCategory> productCategories;


    public MTRequestManager(){

    }

    public MTRequestManager(ItemsListCallBackInterface listInterface){
        this.listInterface = listInterface;
    }

    @Override
    protected Bundle doInBackground(String... params) {
        actionType = params[0];
        dataBundle = new Bundle();
        identifyActionType(actionType,params);
        return dataBundle;
    }

    public void identifyActionType(String actionType, String...params){
        switch (actionType){
            case Actions.MERCHANT_ALL:
                requestUrl = configureURL(MTRequestPath.MT_BASE_URL_SCHEME, MTRequestPath.MT_BASE_URL_HOST, MTRequestPath.MT_MERCHANT_LIST, null);
                dataBundle = processHttpRequest();
                break;
            case Actions.MERCHANT_CATEGORY_PRODUCTS:
                requestUrl = configureURL(MTRequestPath.MT_BASE_URL_SCHEME, MTRequestPath.MT_BASE_URL_HOST, MTRequestPath.MT_MERCHANT_CAT_PRODUCTS, "");
                //enforce a rule that this task should be triggered with the parameters of the merchant id and category id therefore the  params must 3
                if(params.length>2){
                    String merchantId = params[1];
                    String categoryId = params[2];
                    requestUrl += "/"+merchantId+"/"+categoryId;
                    dataBundle = processHttpRequest();
                }else{
                    throw new IllegalStateException("The size of the parameter to run this action isn't accurate, parameter size must be of size three. " +
                            "which includes the merchant_id and category_id with the index in respective order");
                }
                break;
            case Actions.CATEGORY_PRODUCTS_COUNT:
                String path = MTRequestPath.MT_MERCHANT_CAT_PRODUCTS_COUNT;
                path = path.replace("?",params[1]);
                requestUrl = configureURL(MTRequestPath.MT_BASE_URL_SCHEME, MTRequestPath.MT_BASE_URL_HOST, path, null);
                dataBundle = processHttpRequest();
                System.err.println(requestUrl);
                break;
        }
    }


    @Override
    protected void onPostExecute(Bundle s) {
        super.onPostExecute(s);
        if(s.containsKey(MTConnectionConst.CONNECTION_STATUS_KEY)){
            String status = s.getString(MTConnectionConst.CONNECTION_STATUS_KEY);
            if(status!=null) {
                if (status.equalsIgnoreCase(MTConnectionConst.CONNECTION_SUCCESS)) {
                    if(actionType.equalsIgnoreCase(Actions.MERCHANT_ALL)) {
                        listInterface.onConnectionSuccessful(merchants);
                    }
                    else if(actionType.equals(Actions.MERCHANT_CATEGORY_PRODUCTS)) {
                        listInterface.onConnectionSuccessful(products);

                    }else if(actionType.equals(Actions.CATEGORY_PRODUCTS_COUNT)){
                        listInterface.onConnectionSuccessful(productCategories);
                    }
                    Log.i("REQUEST MANAGER", "OnSuccessful has being called");
                } else if (status.equalsIgnoreCase(MTConnectionConst.CONNECTION_FAILED)) {
                    listInterface.onConnectionFailed();
                }
            }
        }
    }

    public Bundle processHttpRequest()
    {
        DefaultHttpClient httpClient; HttpGet httpGet;
        try
        {
            httpClient = new DefaultHttpClient();
            httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,6000);
            httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 10000);
            httpGet = new HttpGet(requestUrl);
            ResponseHandler<String> responseHandler = getResponseHandler();
            httpClient.setHttpRequestRetryHandler(new MTRequestRetryHandler(dataBundle));
            String data = httpClient.execute(httpGet,responseHandler);
            getRetrievedDataProcessor(data);
        }
        catch (IOException io)
        {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
        }
        return dataBundle;
    }

    /**
     * Added this method on 19/12/2014
     * To simplify the code and make it understandable
     * Removed from retrieveMerchants method.
     * @since 2014
     * @return  ResponseHandler
     */
    public ResponseHandler<String> getResponseHandler()
    {
        return new ResponseHandler<String>() {
            @Override
            public String handleResponse(HttpResponse response) throws IOException {
                if(handleResponseCode(response)){
                    return retrieveResponseContent(response);
                }
                return "{\"status\":\"failed\"}";
            }
        };
    }

    /**
     *
     * @param response
     * @return boolean
     */
    public boolean handleResponseCode(HttpResponse response) {
        StatusLine statusLine = response.getStatusLine();
        if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_SUCCESS);
            return true;
        } else {
            dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
            //System.out.println(statusLine.getStatusCode() + " : " + statusLine.getReasonPhrase());
            return false;
        }
    }

    /**
     *
     * @param response      The response received from the server
     *
     * @throws IOException  Throws IOException when reading the entities or content
     *                      or content of the response.
     *
     * @since 2014
     */
    private String retrieveResponseContent(HttpResponse response) throws IOException
    {
        String jsonData; BufferedReader reader; HttpEntity entity;
        StringBuilder builder = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        entity = response.getEntity();
        InputStreamReader streamReader = new InputStreamReader(entity.getContent());
        reader = new BufferedReader(streamReader);
        while((jsonData=reader.readLine())!=null)
        {
            jsonData += newLine;
            builder.append(jsonData);
        }
        streamReader.close();
        reader.close();
        return builder.toString();
    }

    /**
     * By MobileTeller.com
     *
     * @param scheme    This represent the protocol used e.g http, https
     * @param host      The host name. e.g www.mobileteller.com
     *
     *                  note that you should specify the port within the host
     *                  if you intend using a different port other than the
     *                  default port 80. e.g host : www.mobileteller.com:8080
     *
     * @param path      Path accessed within the host when using trailing slashes
     *                  ensure to encode the path before building with the url
     *                  builder
     *
     * @param query
     * @return          returns the string format of the built url
     */
    public String configureURL(String scheme,String host, String path, String query)
    {
        Uri.Builder builder = new Uri.Builder();
                builder.scheme(scheme)
                .encodedAuthority(host)
                .appendEncodedPath(path);
        return builder.build().toString();
    }

    public void getRetrievedDataProcessor(String data)
    {
        MTMerchantDataProcessor dataProcessor = new MTMerchantDataProcessor();
        switch (actionType) {
            case Actions.MERCHANT_ALL:
                this.merchants = dataProcessor.processMerchantData(data);
                break;
            case Actions.MERCHANT_CATEGORY_PRODUCTS:
                try {
                    this.products = dataProcessor.requestProductProcessor(data);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    dataBundle.putString(MTConnectionConst.CONNECTION_STATUS_KEY, MTConnectionConst.CONNECTION_FAILED);
                }
                break;
            case Actions.CATEGORY_PRODUCTS_COUNT:
                this.productCategories = dataProcessor.getCategoryProductCount(data);
                break;
        }
    }

    /**
     *
     * @param callingActivity
     */
    public void setCallBackReceiver(MTMerchantCallBackInterface callingActivity)
    {
        this.callingActivity = callingActivity;
    }
}