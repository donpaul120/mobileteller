package com.nnamdi.mobileteller.Fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.nnamdi.mobileteller.Adapters.MTProductListAdapter;
import com.nnamdi.mobileteller.Components.MTMerchantInfoDialog;
import com.nnamdi.mobileteller.Interfaces.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.POJO.Merchants.MTMerchantDataKey;
import com.nnamdi.mobileteller.POJO.Products.MTProduct;
import com.nnamdi.mobileteller.POJO.Products.MTProductDataKey;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;


/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class MTProductsFragment extends ListFragment implements AbsListView.OnItemClickListener {



    private MTFragmentCommunicationInterface mListener;
    private Activity actionBarActivity;
    private View rootView;
    private PopupWindow merchantInfoWindow;
    private boolean mtPopUpClick = true;

    /**
     * The fragment's ListView/GridView.
     */
    private ListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private MTProductListAdapter mAdapter;

    public static MTProductsFragment newInstance(Bundle productBundle) {
        MTProductsFragment fragment = new MTProductsFragment();
        fragment.setArguments(productBundle);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MTProductsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }
        actionBarActivity = this.getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_product, container, false);
        ImageButton btn = (ImageButton) rootView.findViewById(R.id.info_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mtPopUpClick) {
                    initPopUp();
                    mtPopUpClick =false;
                }else {
                    System.out.println("DISMISSED");
//                    merchantInfoWindow.dismiss();
                    mtPopUpClick = true;
                }
            }
        });
        // Set the adapter
        ArrayList<MTProduct> productList = getArguments().getParcelableArrayList(MTProductDataKey.PRODUCTS);
        mAdapter = new MTProductListAdapter(actionBarActivity.getApplicationContext(),productList);
        mListView = (ListView) rootView.findViewById(android.R.id.list);
        mListView.setDivider(new ColorDrawable(Color.parseColor("#AF939393")));
        mListView.setDividerHeight(1);
        mListView.setAdapter(mAdapter);

        //Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return rootView;
    }


    public void initPopUp()
    {
        try {
            LayoutInflater inflater = (LayoutInflater) actionBarActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View lay = inflater.inflate(R.layout.mt_merchant_details_pop, null);
            Bundle merchantDetails = this.getArguments().getBundle(MTMerchantDataKey.MERCHANT);
            new MTMerchantInfoDialog(actionBarActivity.getApplicationContext(), merchantDetails, lay, rootView);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MTFragmentCommunicationInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.triggerAction("",2,new Bundle());
        }
    }


}
