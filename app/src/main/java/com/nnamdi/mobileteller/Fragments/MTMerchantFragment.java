package com.nnamdi.mobileteller.Fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.nnamdi.mobileteller.Adapters.MTMerchantListAdapter;
import com.nnamdi.mobileteller.Constants.Actions;
import com.nnamdi.mobileteller.Interfaces.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.POJO.Merchants.MTMerchant;
import com.nnamdi.mobileteller.POJO.Merchants.MTMerchantDataKey;
import com.nnamdi.mobileteller.POJO.Products.MTProduct;
import com.nnamdi.mobileteller.POJO.Products.MTProductDataKey;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Paul Okeke
 * Project Leader Nnamdi Jibunoh
 * @since 2014
 *
 * Created on 12th November 2014
 *
 */


public class MTMerchantFragment extends ListFragment implements AbsListView.OnItemClickListener{



    private MTFragmentCommunicationInterface mListener;
    private MTMerchantListAdapter MTMerchantListAdapter;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MTMerchantFragment() {
    }


    public static MTMerchantFragment newInstance(Bundle data)
    {
        MTMerchantFragment mtMerchantFragment = new MTMerchantFragment();
        mtMerchantFragment.setArguments(data);
        return mtMerchantFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setMTMerchantListAdapter(ArrayList<MTMerchant> mtMerchants)
    {
        MTMerchantListAdapter = new MTMerchantListAdapter(mtMerchants,this.getActivity().getApplicationContext());
    }

    public MTMerchantListAdapter getMTMerchantListAdapter() {
        return MTMerchantListAdapter;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MTFragmentCommunicationInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement MTFragmentCommunicationInterface");
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //@MobileTeller : TODO : We need to handle the listView During Screen Config Change

        ArrayList<MTMerchant> mtMerchantArrayList = this.getArguments().getParcelableArrayList(Actions.MERCHANT_KEY);
        setMTMerchantListAdapter(mtMerchantArrayList);
        setListAdapter(getMTMerchantListAdapter());
        //@MobileTeller : We Programatically set the List Divider
        getListView().setDivider(new ColorDrawable(Color.TRANSPARENT));
        getListView().setDividerHeight(25);
        //@MobileTeller : We set the item click listener
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            //@MobileTeller : TODO : Properly Do a Documentation of what is happening here
            Object merchantItemClicked = getListView().getItemAtPosition(position);
            MTMerchant merchant = (MTMerchant) merchantItemClicked;

            System.out.println("USER JUST CLICKED ON : "+position);
            if(position!=82)
            {
                Bundle data = new Bundle();
                Bundle merchantDetails = retrieveMerchantItemDetails(merchant);
                ArrayList<MTProduct> products = merchant.getMerchantProductDetails();

                data.putParcelableArrayList(MTProductDataKey.PRODUCTS, products);
                data.putBundle(MTMerchantDataKey.MERCHANT, merchantDetails);
                //Notify the active callbacks interface (the activity, if the
                //fragment is attached to one) that an item has been selected.
                mListener.triggerAction(Actions.ITEM_CLICKED, position, data);
            }
        }
    }

    /**
     * Note the essence of this method is to retrieve the details
     * strictly partaking to the merchant. Since there appears
     * to be either a memory lag when passing both the merchant object
     * and the product details of the merchant and resulting to also null pointer
     * exception when attempting to retrieve the product details of the merchant
     * Neither is there a possibility
     * that the product details of the merchant
     *
     * @return
     * @since 2014
     * 2:37am (bosun's house) 23/12/2014
     */
    public Bundle retrieveMerchantItemDetails(MTMerchant merchant)
    {
        Bundle merchantDetails = new Bundle();
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_ID, merchant.getMerchantID());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_EMAIL, merchant.getMerchantEmail());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_FULL_BISNAME, merchant.getMerchantFullBisName());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_SHORT_BISNAME, merchant.getMerchantShortBisName());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_RC_NUMBER, merchant.getMerchantRcNumber());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_FB_HANDLE, merchant.getMerchantFacebookHandle());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_TWITTER_HANDLE, merchant.getMerchantTwitterHandle());
        merchantDetails.putInt(MTMerchantDataKey.MERCHANT_ACCT_STATUS, merchant.getAccountStatus());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_WEBSITE, merchant.getMerchantWebsite());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_API_ID, merchant.getMerchantApiId());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_PHONE_OFFICE, merchant.getMerchantPhoneOffice());
        return merchantDetails;
    }

}