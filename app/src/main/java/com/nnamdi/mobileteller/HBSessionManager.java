package com.nnamdi.mobileteller;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;



/**
 * @author Paul Okeke
 * Created by paulex10 on 29/04/2015.
 */
public class HBSessionManager {


    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    private Context context;

    public static final String PREF_NAME="com.mobileteller.app.pref";


    public static final String SESSION_TOKEN = "session_token";


    public HBSessionManager(Context context)
    {
        this.context = context;
        sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        editor.apply();
    }

//    public void createLoginSession(String username, String password, String token, int userID)
//    {
//        editor.putString(USER_EMAIL_KEY, username);
//        editor.putString(USER_PASSWORD_KEY,password);
//        editor.putString(SESSION_TOKEN, token);
//        editor.putInt(USER_ID_KEY, userID);
//        editor.putBoolean(IS_LOGGED_IN, true);
//        editor.putString(APPOINTMENTS_KEY, null);
//        editor.commit();
//        //System.out.println("Session started and created :" + sharedPref.getString(SESSION_TOKEN, "") );
//    }

    public String getSessionToken()
    {
        return sharedPref.getString(SESSION_TOKEN, null);
    }



    public SharedPreferences getSharedPref()
    {
        return sharedPref;
    }

    public void put(String key, String value){
        editor.putString(key, value);
        editor.commit();
    }

    public void clearSession()
    {
        editor.clear();
        editor.commit();
    }
}