package com.nnamdi.mobileteller.Merchant.Interface;

import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;

/**
 * Created by paulex10 on 16/06/2015.
 */
public interface MTMerchantCategoryCallBackInterface {

    void requestForCategoryProductsCount(ItemsListCallBackInterface callback, String merchantId);
}
