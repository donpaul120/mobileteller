package com.nnamdi.mobileteller.Merchant.Utility;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * Created by paulex10 on 20/12/2014.
 */
public class MTMerchant implements Parcelable{

    private String merchantID;
    private String merchantEmail;
    private String merchantFullBisName;
    private String merchantShortBisName;
    private String merchantRcNumber;
    private String merchantLogoUrl;
    private String merchantFacebookHandle;
    private String merchantTwitterHandle;
    private String merchantWebsite;
    private String merchantApiId;
    private String merchantPhoneOffice;
    private String merchantPhoneAlternate;
    private String merchantProductCategoriesJson;
    private String merchantBusinessHoursJson;
    private String merchantDescriptiveImageUrl;
    private String merchantStreet;
    private int accountStatus;


    public MTMerchant()
    {
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantFullBisName(String merchantFullBisName) {
        this.merchantFullBisName = merchantFullBisName;
    }

    public String getMerchantFullBisName() {
        return merchantFullBisName;
    }

    public void setMerchantShortBisName(String merchantShortBisName) {
        this.merchantShortBisName = merchantShortBisName;
    }

    public String getMerchantShortBisName() {
        return merchantShortBisName;
    }

    public void setMerchantRcNumber(String merchantRcNumber) {
        this.merchantRcNumber = merchantRcNumber;
    }

    public String getMerchantRcNumber() {
        return merchantRcNumber;
    }

    public void setMerchantLogoUrl(String merchantLogoUrl) {
        this.merchantLogoUrl = merchantLogoUrl;
    }

    public String getMerchantLogoUrl() {
        return merchantLogoUrl;
    }

    public String getMerchantDescriptiveImageUrl() {
        return merchantDescriptiveImageUrl;
    }

    public void setMerchantDescriptiveImageUrl(String merchantDescriptiveImageUrl) {
        this.merchantDescriptiveImageUrl = merchantDescriptiveImageUrl;
    }

    public void setMerchantFacebookHandle(String merchantFacebookHandle) {
        this.merchantFacebookHandle = merchantFacebookHandle;
    }

    public String getMerchantFacebookHandle() {
        return merchantFacebookHandle;
    }

    public void setMerchantTwitterHandle(String merchantTwitterHandle) {
        this.merchantTwitterHandle = merchantTwitterHandle;
    }

    public void setAccountStatus(int accountStatus) {
        this.accountStatus = accountStatus;
    }

    public int getAccountStatus() {
        return accountStatus;
    }

    public String getMerchantTwitterHandle() {
        return merchantTwitterHandle;
    }

    public void setMerchantWebsite(String merchantWebsite) {
        this.merchantWebsite = merchantWebsite;
    }

    public String getMerchantWebsite() {
        return merchantWebsite;
    }

    public void setMerchantApiId(String merchantApiId) {
        this.merchantApiId = merchantApiId;
    }

    public String getMerchantApiId() {
        return merchantApiId;
    }

    public void setMerchantPhoneOffice(String merchantPhoneOffice) {
        this.merchantPhoneOffice = merchantPhoneOffice;
    }

    public String getMerchantPhoneOffice() {
        return merchantPhoneOffice;
    }

    public void setMerchantPhoneAlternate(String merchantPhoneAlternate) {
        this.merchantPhoneAlternate = merchantPhoneAlternate;
    }

    public String getMerchantPhoneAlternate() {
        return merchantPhoneAlternate;
    }

    public String getMerchantProductCategoriesJson() {
        return merchantProductCategoriesJson;
    }

    public void setMerchantProductCategoriesJson(String merchantProductCategoriesJson) {
        this.merchantProductCategoriesJson = merchantProductCategoriesJson;
    }


    public String getMerchantBusinessHoursJson() {
        return merchantBusinessHoursJson;
    }

    public void setMerchantBusinessHoursJson(String merchantBusinessHoursJson) {
        this.merchantBusinessHoursJson = merchantBusinessHoursJson;
    }


    public MTMerchant(Parcel parcel)
    {
        this.merchantID = parcel.readString();
        this.merchantEmail = parcel.readString();
        this.merchantFullBisName = parcel.readString();
        this.merchantShortBisName = parcel.readString();
        this.merchantRcNumber = parcel.readString();
        this.merchantLogoUrl = parcel.readString();
        this.merchantFacebookHandle = parcel.readString();
        this.merchantTwitterHandle = parcel.readString();
        this.merchantWebsite = parcel.readString();
        this.merchantPhoneOffice = parcel.readString();
        this.merchantPhoneAlternate = parcel.readString();
        this.merchantProductCategoriesJson = parcel.readString();
        this.merchantDescriptiveImageUrl = parcel.readString();
        this.merchantStreet = parcel.readString();
    }

    public static final Creator<MTMerchant> CREATOR = new Creator<MTMerchant>() {

        @Override
        public MTMerchant createFromParcel(Parcel source) {
            return new MTMerchant(source);
        }

        @Override
        public MTMerchant[] newArray(int size) {
            return new MTMerchant[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int flags) {
        destination.writeString(merchantID);
        destination.writeString(merchantEmail);
        destination.writeString(merchantFullBisName);
        destination.writeString(merchantShortBisName);
        destination.writeString(merchantRcNumber);
        destination.writeString(merchantLogoUrl);
        destination.writeString(merchantFacebookHandle);
        destination.writeString(merchantTwitterHandle);
        destination.writeString(merchantPhoneOffice);
        destination.writeString(merchantPhoneAlternate);
        destination.writeString(merchantProductCategoriesJson);
        destination.writeString(merchantDescriptiveImageUrl);
        destination.writeString(merchantStreet);
        //destination.writeString(merchantBusinessHoursJson);
    }

    public String getMerchantStreet() {
        return merchantStreet;
    }

    public void setMerchantStreet(String merchantStreet) {
        this.merchantStreet = merchantStreet;
    }
}
