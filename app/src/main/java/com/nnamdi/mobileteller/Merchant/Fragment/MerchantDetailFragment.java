package com.nnamdi.mobileteller.Merchant.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Merchant.Adapter.FragmentPagerAdapter;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.R;
import com.nnamdi.mobileteller.lib.SlidingTabLayout;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MerchantDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MerchantDetailFragment extends Fragment {

    private ViewPager viewPager;
    private Context context;
    private TextView mtMerchantOpenClosed;
    private ImageView merchantLogo;
    private LinearLayout merchantClosedInfoCont;
    public static String TAG ="MERCHANT_DETAIL_ACTIVITY" ;
    private  Handler handler;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MerchantDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MerchantDetailFragment newInstance(Bundle bundle) {
        MerchantDetailFragment fragment = new MerchantDetailFragment();
        Bundle args = new Bundle();
        args.putString(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES, bundle.getString(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES));
        args.putString(MTMerchantDataKey.MERCHANT_ID, bundle.getString(MTMerchantDataKey.MERCHANT_ID));
        args.putBundle(MTMerchantDataKey.MERCHANT, bundle.getBundle(MTMerchantDataKey.MERCHANT));
        fragment.setArguments(args);
        return fragment;
    }

    public MerchantDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateClosedInfoView();
            }
        }, 5000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_merchant_detail, container, false);
        setMerchantLogo((ImageView) rootView.findViewById(R.id.merchant_logo));
        setViewPager((ViewPager) rootView.findViewById(R.id.mt_view_pager));
        setSlideTab((SlidingTabLayout) rootView.findViewById(R.id.mt_sliding_tab));
        setMerchantClosedInfoCont((LinearLayout) rootView.findViewById(R.id.closed_info_cont));
        setMtMerchantName((TextView) rootView.findViewById(R.id.mt_merchant_name));
        setMtMerchantOpenClosed((TextView) rootView.findViewById(R.id.mt_merchant_open_closed));
        ViewCompat.setTransitionName(merchantLogo, "EXTRA_IMAGE");
        View views [] = {mtMerchantOpenClosed};
        FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);
        return rootView;
    }

    public void animateClosedInfoView(){
        if(merchantClosedInfoCont!=null){
            Animation animation = new TranslateAnimation(0, -merchantClosedInfoCont.getWidth(),0,0);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.setFillAfter(true);
            animation.setFillEnabled(true);
            animation.setDuration(1000);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Animation animation1 = new TranslateAnimation(-merchantClosedInfoCont.getWidth(),-merchantClosedInfoCont.getWidth(), 0, -400.f);
                    animation1.setInterpolator(new AnticipateOvershootInterpolator(4));
                    animation1.setFillAfter(true);
                    animation1.setFillEnabled(true);
                    animation1.setDuration(3000);
                    merchantClosedInfoCont.startAnimation(animation1);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
           // merchantClosedInfoCont.startAnimation(animation);
        }
    }


    /**
     * We set the sliding tab here
     * @param slideTab  This is a customized library of the sliding tab_layout
     */
    public void setSlideTab(SlidingTabLayout slideTab) {
        int  drawables[] = {R.drawable.tab_icon_category, R.drawable.tab_icon_location, R.drawable.tab_icon_profile};
        int[] info_graphics = {R.drawable.merch_4, R.drawable.food1, R.drawable.chicken_jafrezzi2};
        slideTab.setImage_drawables(drawables);
        slideTab.setTabInfoGraphicsDrawable(this.merchantLogo, info_graphics);
        slideTab.setCustomTabView(R.layout.mt_custom_tab_layout, R.id.mt_tab_icon);
        slideTab.setFadeInOutAnimation(new AlphaAnimation(0.3f, 1));
        //this.slideTab.setDividerColors();
        //this.slideTab.setSelectedIndicatorColors(Color.parseColor("#ff2929"));
        slideTab.setViewPager(this.viewPager);
    }

    boolean status=true;
    public void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        ArrayList<Fragment> fragments = new ArrayList<>();
        String merchantId = getArguments().getString(MTMerchantDataKey.MERCHANT_ID);
        fragments.add(MerchantProductCategoriesFragment.newInstance(merchantId ,getArguments().getString(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES)));
        fragments.add(MerchantLocationFragment.newInstance("",""));
        fragments.add(MerchantProfileFragment.newInstance(getArguments().getBundle(MTMerchantDataKey.MERCHANT)));
        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getActivity().getSupportFragmentManager());
        fragmentPagerAdapter.setFragments(fragments);
        this.viewPager.setAdapter(fragmentPagerAdapter);
    }

    public void setMerchantLogo(ImageView merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    public void setMtMerchantName(TextView mtMerchantName) {
        FontStyleFactory.setFontForView(context, mtMerchantName, StyleConstants.BOLD_ITALIC_FONT);
    }

    public void setMtMerchantOpenClosed(TextView mtMerchantOpenClosed) {
        this.mtMerchantOpenClosed = mtMerchantOpenClosed;
    }

    public void setMerchantClosedInfoCont(LinearLayout merchantClosedInfoCont) {
        this.merchantClosedInfoCont = merchantClosedInfoCont;
    }
}