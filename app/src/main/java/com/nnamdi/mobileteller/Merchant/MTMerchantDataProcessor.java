package com.nnamdi.mobileteller.Merchant;

import com.nnamdi.mobileteller.Merchant.Utility.MTMerchant;
import com.nnamdi.mobileteller.Merchant.Utility.MerchantProductCategory;
import com.nnamdi.mobileteller.Products.Utility.MTProduct;
import com.nnamdi.mobileteller.Products.MTProductDataProcessor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * MobileTeller.com
 * @author Paul Okeke
 * Created by paulex10 on 20/12/2014.
 */
public class MTMerchantDataProcessor {
    //String[] draws[] = {R.drawable.merch_4};

    /**
     * By MobileTeller.com
     *
     * 04/12/2014 9:30PM
     * @since 2014
     * @param mtMerchantJson This is the jsonArray received from MobileTeller
     */
    public ArrayList<MTMerchant> processMerchantData(String mtMerchantJson)
    {
        ArrayList<MTMerchant> merchantsList = null;
        if(!mtMerchantJson.isEmpty())
            try {
                System.out.println("DATA:::::: "+mtMerchantJson);
                 merchantsList = new ArrayList<>();
                //retrieve the whole products array
                JSONArray merchantsArray = new JSONArray(mtMerchantJson);
                for (int i = 0; i < merchantsArray.length(); i++) {
                    //@MobileTeller retrieves a single merchant object from the array
                   retrieveMTMerchantDetails(merchantsArray.getJSONObject(i), merchantsList);
                }
                //dataBundle.putParcelableArrayList(Actions.MERCHANT_KEY, merchantsList);
            } catch (JSONException jsonEx) {
                System.out.println(jsonEx.getMessage());
            }
        return merchantsList;
    }

    /**
     *
     * @param merchantObject    A particular json object of a merchant
     *
     */
    public void retrieveMTMerchantDetails(JSONObject merchantObject, ArrayList<MTMerchant> merchantsList)
    {

        MTMerchant merchant = new MTMerchant();
        try {
            //merchant.setMerchantLogoUrl(draws[0]);
            merchant.setMerchantID(merchantObject.getString(MTMerchantDataKey.MERCHANT_ID));
            merchant.setMerchantEmail(merchantObject.getString(MTMerchantDataKey.MERCHANT_EMAIL));
            merchant.setMerchantFullBisName(merchantObject.getString(MTMerchantDataKey.MERCHANT_FULL_BISNAME));
            merchant.setMerchantShortBisName(merchantObject.getString(MTMerchantDataKey.MERCHANT_SHORT_BISNAME));
            merchant.setMerchantRcNumber(merchantObject.getString(MTMerchantDataKey.MERCHANT_RC_NUMBER));
            merchant.setMerchantLogoUrl(merchantObject.getJSONArray(MTMerchantDataKey.MERCANT_PICTURE_DETAILS).getJSONObject(1).getString("url"));
            merchant.setMerchantFacebookHandle(merchantObject.getString(MTMerchantDataKey.MERCHANT_FB_HANDLE));
            merchant.setMerchantTwitterHandle(merchantObject.getString(MTMerchantDataKey.MERCHANT_TWITTER_HANDLE));
            merchant.setMerchantWebsite(merchantObject.getString(MTMerchantDataKey.MERCHANT_WEBSITE));
            merchant.setMerchantPhoneOffice(merchantObject.getString(MTMerchantDataKey.MERCHANT_PHONE_OFFICE));
            merchant.setMerchantPhoneAlternate(merchantObject.getString(MTMerchantDataKey.MERCHANT_PHONE_ALT));
            merchant.setMerchantStreet(getValueFromJsonArray("street", merchantObject.getJSONArray(MTMerchantDataKey.MERCHANT_LOCATION_DETAILS), false));
            if(merchantObject.has(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES)){
                merchant.setMerchantProductCategoriesJson(merchantObject.getJSONArray(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES).toString());
            }else{
                System.out.println("Categories Not Found");
            }
            merchantsList.add(merchant);
        }catch (JSONException ex)
        {
            ex.printStackTrace();
        }
    }

    public String getValueFromJsonArray(String key, JSONArray array, boolean all) throws JSONException{
        String value = "";
        if(all) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject locationObject = array.getJSONObject(i);
            }
        }else{
            JSONObject jsonObject = array.getJSONObject(0);
            value = jsonObject.getString(key);
        }
        return value;
    }

    /**
     * since 2014 21/12/2014
     * 1:29AM
     * @throws JSONException
     */
    public ArrayList<MTProduct> requestProductProcessor(String productsJson) throws JSONException
    {
        JSONArray productDetails = new JSONArray(productsJson);
        return new MTProductDataProcessor().processProductData(productDetails);
    }

    public ArrayList<MerchantProductCategory> getCategoryProductCount(String json) {
        ArrayList<MerchantProductCategory> merchantProductCategories = null;
        try {
            JSONArray categoryArray = new JSONArray(json);
            if (categoryArray.length() > 0) {
                merchantProductCategories = new ArrayList<>();
                for (int i = 0; i < categoryArray.length(); i++) {
                    MerchantProductCategory category = new MerchantProductCategory();
                    JSONObject categoryObj = categoryArray.getJSONObject(i);
                    category.setCategoryId(categoryObj.getString("id"));
                    category.setCategoryProductCount(categoryObj.getInt("_category_count"));
                    merchantProductCategories.add(category);
                }
            }
        }catch (JSONException ex){
            ex.printStackTrace();
        }
        return merchantProductCategories;
    }
}
