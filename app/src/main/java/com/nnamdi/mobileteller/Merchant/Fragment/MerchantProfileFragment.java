package com.nnamdi.mobileteller.Merchant.Fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MerchantProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MerchantProfileFragment extends Fragment {

    private Bundle merchantDetails;
    private TextView merchantName;
    private TextView merchantShortName;
    private TextView merchantEmailAddress;
    private TextView merchantWebsite;
    private TextView merchantRcNumber;
    private TextView merchantFacebookHandle;
    private TextView merchantTwitterHandle;
    private TextView merchantPhone;
    private TextView merchantAltPhone;

    private Context context;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param merchantDetail Parameter 2.
     * @return A new instance of fragment MerchantProfileFragment.
     */
    public static MerchantProfileFragment newInstance(Bundle merchantDetail) {
        MerchantProfileFragment fragment = new MerchantProfileFragment();
        Bundle args = new Bundle();
        args.putBundle(MTMerchantDataKey.MERCHANT, merchantDetail);
        fragment.setArguments(args);
        return fragment;
    }

    public MerchantProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().containsKey(MTMerchantDataKey.MERCHANT)){
                merchantDetails = getArguments().getBundle(MTMerchantDataKey.MERCHANT);
            }
        }
        this.context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_merchant_profile, container, false);
        setMerchantName((TextView) rootView.findViewById(R.id.mt_merchant_name));
        setMerchantShortName((TextView) rootView.findViewById(R.id.mt_merchant_short_name));
        setMerchantEmailAddress((TextView) rootView.findViewById(R.id.mt_merchant_email));
        setMerchantWebsite((TextView) rootView.findViewById(R.id.mt_merchant_website));
        setMerchantRcNumber((TextView) rootView.findViewById(R.id.mt_merchant_rc_no));
        setMerchantFacebookHandle((TextView) rootView.findViewById(R.id.mt_merchant_facebook_handle));
        setMerchantTwitterHandle((TextView) rootView.findViewById(R.id.mt_merchant_twitter));
        setMerchantPhone((TextView) rootView.findViewById(R.id.mt_merchant_phone));
        setMerchantAltPhone((TextView) rootView.findViewById(R.id.mt_merchant_alt_phone));
        setMerchantCallerBtn((ImageButton) rootView.findViewById(R.id.merchant_phone_caller));
        setMerchantEmailSendBtn((ImageButton) rootView.findViewById(R.id.merchant_email_send));

        View views[] = {merchantName, merchantShortName, merchantEmailAddress,
                merchantWebsite, merchantRcNumber, merchantFacebookHandle,
                merchantTwitterHandle, merchantPhone, merchantAltPhone};

        FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);
        return rootView;
    }

    public void setMerchantName(TextView merchantName) {
        this.merchantName = merchantName;
        this.merchantName.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_FULL_BISNAME));
    }

    public void setMerchantShortName(TextView merchantShortName) {
        this.merchantShortName = merchantShortName;
        this.merchantShortName.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_SHORT_BISNAME));
    }

    public void setMerchantEmailAddress(TextView merchantEmailAddress) {
        this.merchantEmailAddress = merchantEmailAddress;
        this.merchantEmailAddress.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_EMAIL));
    }

    public void setMerchantWebsite(TextView merchantWebsite) {
        this.merchantWebsite = merchantWebsite;
        this.merchantWebsite.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_WEBSITE));
    }

    public void setMerchantRcNumber(TextView merchantRcNumber) {
        this.merchantRcNumber = merchantRcNumber;
        this.merchantRcNumber.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_RC_NUMBER));
    }

    public void setMerchantFacebookHandle(TextView merchantFacebookHandle) {
        this.merchantFacebookHandle = merchantFacebookHandle;
        this.merchantFacebookHandle.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_FB_HANDLE));
    }

    public void setMerchantTwitterHandle(TextView merchantTwitterHandle) {
        this.merchantTwitterHandle = merchantTwitterHandle;
        this.merchantTwitterHandle.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_TWITTER_HANDLE));
    }

    public void setMerchantPhone(TextView merchantPhone) {
        this.merchantPhone = merchantPhone;
        this.merchantPhone.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_PHONE_OFFICE));
    }

    public void setMerchantAltPhone(TextView merchantAltPhone) {
        this.merchantAltPhone = merchantAltPhone;
        this.merchantAltPhone.setText(this.merchantDetails.getString(MTMerchantDataKey.MERCHANT_PHONE_ALT));
    }

    public void setMerchantCallerBtn(ImageButton merchantCallerBtn) {
        merchantCallerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCall = new Intent(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse("tel:" + merchantPhone.getText()));
                startActivity(intentCall);
            }
        });
    }

    public void setMerchantEmailSendBtn(ImageButton merchantEmailSendBtn){
        merchantEmailSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String receiver [] = {merchantEmailAddress.getText().toString()};
                Intent intentMail = new Intent(Intent.ACTION_SEND);
                intentMail.setType("message/rfc822");
                intentMail.putExtra(Intent.EXTRA_SUBJECT,"RE:");
                intentMail.putExtra(Intent.EXTRA_TEXT, "Hello Merchant");
                intentMail.putExtra(Intent.EXTRA_EMAIL, receiver);
                startActivity(intentMail);
            }
        });
    }
}