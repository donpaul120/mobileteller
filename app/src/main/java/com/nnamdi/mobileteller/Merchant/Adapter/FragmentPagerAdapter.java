package com.nnamdi.mobileteller.Merchant.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;

/**
 * @author PAule
 * Created by paulex10 on 18/06/2015.
 */
public class FragmentPagerAdapter extends android.support.v4.app.FragmentPagerAdapter{


    private ArrayList<Fragment> fragments;

    public FragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    public void setFragments(ArrayList<Fragment> fragments){
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "test".toString();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
