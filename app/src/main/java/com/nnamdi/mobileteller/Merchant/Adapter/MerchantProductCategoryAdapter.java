package com.nnamdi.mobileteller.Merchant.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Merchant.Utility.MerchantProductCategory;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 16/06/2015.
 */
public class MerchantProductCategoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<MerchantProductCategory> productCategories;


    public MerchantProductCategoryAdapter(Context context, ArrayList<MerchantProductCategory> productCategories) {
        this.context = context;
        this.productCategories = productCategories;
    }

    @Override
    public int getCount() {
        return productCategories.size();
    }

    @Override
    public Object getItem(int position) {
        return productCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.merchant_product_category_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.categoryName = (TextView) convertView.findViewById(R.id.mt_product_category_name);
            viewHolder.categoryDesc = (TextView) convertView.findViewById(R.id.mt_product_category_desc);
            viewHolder.productCount = (TextView) convertView.findViewById(R.id.category_product_count);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.categoryName.setText(productCategories.get(position).getCategoryName());
        viewHolder.categoryDesc.setText(productCategories.get(position).getCategoryDescription());
        viewHolder.productCount.setText(" "+productCategories.get(position).getCategoryProductCount());
        View views[] = {viewHolder.categoryDesc, viewHolder.productCount};
        FontStyleFactory.setFontForView(context, viewHolder.categoryName, StyleConstants.GENERAL_APP_FONT_BOLD);
        FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);

        return convertView;
    }

    static class ViewHolder{
        TextView categoryName;
        TextView categoryDesc;
        ImageView categoryIcon;
        TextView productCount;
    }
}
