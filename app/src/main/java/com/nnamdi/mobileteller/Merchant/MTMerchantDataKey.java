package com.nnamdi.mobileteller.Merchant;

/**
 * MobileTeller.com
 * @author Paul Okeke
 * 12:46PM
 * Created by paulex10 on 20/12/2014.
 */
public class MTMerchantDataKey {

    //OBJECT KEY
    public static String MERCHANT = "MERCHANT";

    //JSON KEY
    public static String MERCHANT_ID = "id";
    public static String MERCHANT_EMAIL = "email";
    public static String MERCHANT_FULL_BISNAME="full_business_name";
    public static String MERCHANT_SHORT_BISNAME="short_business_name";
    public static String MERCHANT_RC_NUMBER="rc_number";
    public static String MERCHANT_LOGO = "logo";
    public static String MERCHANT_FB_HANDLE = "facebook_handle";
    public static String MERCHANT_TWITTER_HANDLE = "twitter_handle";
    public static String MERCHANT_ACCT_STATUS = "accountStatus";
    public static String MERCHANT_WEBSITE = "website";
    public static String MERCHANT_API_ID = "api_id";
    public static String MERCHANT_PHONE_OFFICE = "phone_office";
    public static String MERCHANT_PHONE_ALT = "phone_alternate";
    public static String MERCHANT_PRODUCT_CATEGORIES= "product_categories";
    public final static String MERCANT_PICTURE_DETAILS =  "picture_details";
    public final static String MERCHANT_LOCATION_DETAILS = "merchant_locations";
    public static String MERCHANT_BANK_DETAILS = "merchantBankDetails";
    public static String MERCHANT_ADDR_DETAILS = "merchantAddressDetails";
    public static String MERCHANT_OWNER_DETAILS = "merchantOwnersDetails";



    public static String CATEGORY_NAME ="category_name";
    public static String CATEGORY_DESC = "category_description";
    public static String CATEGORY_ID = "id";


}
