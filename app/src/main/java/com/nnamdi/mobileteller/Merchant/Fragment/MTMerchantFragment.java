package com.nnamdi.mobileteller.Merchant.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Components.MTListView;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.Constants.ActionType;
import com.nnamdi.mobileteller.Merchant.Activity.MerchantDetailActivity;
import com.nnamdi.mobileteller.Merchant.Adapter.MTMerchantListAdapter;
import com.nnamdi.mobileteller.Constants.Actions;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Merchant.Interface.MTMerchantCallBackInterface;
import com.nnamdi.mobileteller.Merchant.Utility.MTMerchant;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Project Leader Nnamdi Jibunoh
 * @since 2014
 *
 * Created on 12th November 2014
 *
 */


public class MTMerchantFragment extends Fragment implements AbsListView.OnItemClickListener, ItemsListCallBackInterface, SwipeRefreshLayout.OnRefreshListener {


    public final static String TAG = "MERCHANT_FRAGMENT";
    private View rootView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MTFragmentCommunicationInterface mListener;
    private MTMerchantListAdapter MTMerchantListAdapter;
    private Context context;
    private ArrayList<MTMerchant> mtMerchantArrayList;
    private ListView mtMerchantListView;
    private ImageCacheInterface cacheInterface;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MTMerchantFragment() {
    }


    public static MTMerchantFragment newInstance(Bundle data) {
        return new MTMerchantFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = LayoutInflater.from(context).inflate(R.layout.fragment_merchant, container, false);
        mtMerchantListView = (ListView) rootView.findViewById(R.id.merchant_list);
        mtMerchantListView.addHeaderView(LayoutInflater.from(context).inflate(R.layout.header_view, mtMerchantListView, false));
        createSwipeRefreshLayout();
        setListViewScrollListener();
        MTListView.dispatchScrollEvent(mtMerchantListView, mListener);
        return rootView;
    }

    public void setListViewScrollListener(){
        mtMerchantListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    System.out.println("Still Holding ");
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //Toast.makeText(context,mtMerchantListView.getChildAt(0).getTop()+"",Toast.LENGTH_SHORT ).show();
                if(mtMerchantListView.getChildAt(0)!=null) {
                    if (firstVisibleItem == 0 && visibleItemCount>0 && mtMerchantListView.getChildAt(0).getTop() >= 0) {
                        swipeRefreshLayout.setEnabled(true);
                    } else {
                        swipeRefreshLayout.setEnabled(false);
                    }
                }
            }
        });
    }

    public void createSwipeRefreshLayout(){
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setProgressViewOffset(false, 0, 100);
        int colors[] ={Color.parseColor("#03A9F4"), Color.parseColor("#FF9100")};
        swipeRefreshLayout.setColorSchemeColors(colors);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setFocusable(false);
    }

    public void setMTMerchantListAdapter(ArrayList<MTMerchant> mtMerchants) {
        MTMerchantListAdapter = new MTMerchantListAdapter(mtMerchants, this.getActivity().getApplicationContext(), cacheInterface);
    }

    public MTMerchantListAdapter getMTMerchantListAdapter() {
        return MTMerchantListAdapter;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MTFragmentCommunicationInterface) activity;
            cacheInterface = (ImageCacheInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MTFragmentCommunicationInterface");
        }
    }

    public void initializeListView() {
        if (mtMerchantArrayList == null) {
            mtMerchantArrayList = new ArrayList<>();//this.getArguments().getParcelableArrayList(Actions.MERCHANT_KEY);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mtMerchantArrayList != null) {
                outState.putParcelableArrayList(MTMerchantDataKey.MERCHANT, mtMerchantArrayList);
            //Toast.makeText(context,"I Have saved my data",Toast.LENGTH_SHORT).show();
        }else{
            //Toast.makeText(context,"List is Null",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            if(savedInstanceState.containsKey(MTMerchantDataKey.MERCHANT)) {
                try {
                    mtMerchantArrayList = savedInstanceState.getParcelableArrayList(MTMerchantDataKey.MERCHANT);
                } catch (Exception jex) {
                    jex.printStackTrace();
                }
               // Toast.makeText(context,"Found the Key",Toast.LENGTH_SHORT).show();
            }
        }
        initializeListView();
        setMTMerchantListAdapter(mtMerchantArrayList);
        mtMerchantListView.setAdapter(MTMerchantListAdapter);
        //@MobileTeller : We Programatically set the List Divider
        mtMerchantListView.setDivider(new ColorDrawable(Color.TRANSPARENT));
        mtMerchantListView.setDividerHeight(18);
        mtMerchantListView.setPadding(10, 0, 10, 0);
        //@MobileTeller : We set the item click listener
        mtMerchantListView.setOnItemClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            //@MobileTeller : TODO : Properly Do a Documentation of what is happening here

            MTMerchant merchant = (MTMerchant) mtMerchantListView.getItemAtPosition(position);

            //System.out.println("USER JUST CLICKED ON : "+merchant.getMerchantProductCategoriesJson());
            if (position != 82) {
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation
                        (this.getActivity(), view.findViewById(R.id.merchant_main), "EXTRA_IMAGE");
                Intent intent = new Intent(context, MerchantDetailActivity.class);
                intent.putExtra(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES, merchant.getMerchantProductCategoriesJson());
                intent.putExtra(MTMerchantDataKey.MERCHANT_ID, merchant.getMerchantID());
                intent.putExtra(MTMerchantDataKey.MERCHANT, retrieveMerchantItemDetails(merchant));
                ActivityCompat.startActivity(this.getActivity(), intent, options.toBundle());
            }
        }
    }

    /**
     * Note the essence of this method is to retrieve the details
     * strictly partaking to the merchant. Since there appears
     * to be either a memory lag when passing both the merchant object
     * and the product details of the merchant and resulting to also null pointer
     * exception when attempting to retrieve the product details of the merchant
     * Neither is there a possibility
     * that the product details of the merchant
     *
     * @return Bundle
     * @since 2014
     * 2:37am (bosun's house) 23/12/2014
     */
    public Bundle retrieveMerchantItemDetails(MTMerchant merchant) {
        Bundle merchantDetails = new Bundle();
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_ID, merchant.getMerchantID());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_EMAIL, merchant.getMerchantEmail());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_FULL_BISNAME, merchant.getMerchantFullBisName());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_SHORT_BISNAME, merchant.getMerchantShortBisName());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_RC_NUMBER, merchant.getMerchantRcNumber());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_FB_HANDLE, merchant.getMerchantFacebookHandle());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_TWITTER_HANDLE, merchant.getMerchantTwitterHandle());
        merchantDetails.putInt(MTMerchantDataKey.MERCHANT_ACCT_STATUS, merchant.getAccountStatus());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_WEBSITE, merchant.getMerchantWebsite());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_API_ID, merchant.getMerchantApiId());
        merchantDetails.putString(MTMerchantDataKey.MERCHANT_PHONE_OFFICE, merchant.getMerchantPhoneOffice());
        return merchantDetails;
    }

    @Override
    public void onConnectionFailed() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onConnectionSuccessful(ArrayList<?> items) {
        if (items != null) {
            @SuppressWarnings("unchecked")
            ArrayList<MTMerchant> merchants = (ArrayList<MTMerchant>) items;//data.getParcelableArrayList(Actions.MERCHANT_KEY);
            //if(merchants!=null) {
            //Toast.makeText(context, " "+merchants.size()+"",Toast.LENGTH_SHORT).show();
            mtMerchantArrayList.clear();
            for (MTMerchant merchant : merchants) {
                mtMerchantArrayList.add(merchant);
            }
            MTMerchantListAdapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        mListener.triggerAction(ActionType.LIST_DATA_REFRESH,0, null);
    }
}