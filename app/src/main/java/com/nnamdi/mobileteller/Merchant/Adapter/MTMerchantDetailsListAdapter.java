package com.nnamdi.mobileteller.Merchant.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 21/12/2014.
 */

public class MTMerchantDetailsListAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<String> merchantDetialList;
    private int count;

    public MTMerchantDetailsListAdapter(Context context,ArrayList<String> merchantDetialList)
    {
        this.context = context;
        this.merchantDetialList =  merchantDetialList;
    }

    @Override
    public int getCount() {
        return merchantDetialList.size();
    }

    @Override
    public Object getItem(int position) {
        return merchantDetialList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.mt_merchant_detail_list_item,parent,false);

            ViewHolder viewHolder = new ViewHolder();
        viewHolder.detailItem = (TextView) convertView.findViewById(R.id.mt_pop_merchant_detail_title);
        viewHolder.detailIcon = (ImageView) convertView.findViewById(R.id.mt_pop_merchant_detail_ic);

        String title = merchantDetialList.get(position);
        if(position==0)
        {
            setMerchantDetailFacebookHandle(viewHolder, title);
        }
        else if(position==1)
        {
            setMerchantDetailTwitterHandle(viewHolder, title);
        }
        convertView.setTag(viewHolder);

        return convertView;
    }

    public void setMerchantDetailFacebookHandle(ViewHolder viewHolder, String title)
    {
        viewHolder.detailItem.setText(title);
        viewHolder.detailIcon.setImageResource(R.drawable.fb_ic);
        viewHolder.detailItem.setTypeface(Typeface.createFromAsset(context.getAssets(), "MobileTellerGen.otf"));
    }

    public void setMerchantDetailTwitterHandle(ViewHolder viewHolder, String title)
    {
        viewHolder.detailItem.setText(title);
        //viewHolder.detailIcon.setImageResource(R.drawable.twit_ic);
        viewHolder.detailItem.setTypeface(Typeface.createFromAsset(context.getAssets(), "MobileTellerGen.otf"));
    }

    static class ViewHolder
    {
        TextView detailItem;
        ImageView detailIcon;
    }
}
