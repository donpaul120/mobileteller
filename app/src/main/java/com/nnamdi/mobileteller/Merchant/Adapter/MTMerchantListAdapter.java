package com.nnamdi.mobileteller.Merchant.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nnamdi.mobileteller.Components.MTImageHelper;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.ConnectionPool.MTImageLoader;
import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Merchant.Utility.MTMerchant;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

/**
 * @author Paul Okeke
 * Created by paulex10 on 12/11/2014.
 */
public class MTMerchantListAdapter extends BaseAdapter{

    private ArrayList<MTMerchant> mtMerchants;
    private Context context;
    private ImageCacheInterface imageCacheInterface;


    public MTMerchantListAdapter(ArrayList<MTMerchant> mtMerchants, Context context, ImageCacheInterface imageCacheInterface){
        this.context = context;
        this.mtMerchants = mtMerchants;
        this.imageCacheInterface = imageCacheInterface;
    }

    @Override
    public int getCount() {
        return mtMerchants.size();
    }

    @Override
    public Object getItem(int i) {
        return mtMerchants.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
		if(convertView==null){
			convertView = LayoutInflater.from(context).inflate(R.layout.merchant_item, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.merchantLogo = (ImageView) convertView.findViewById(R.id.merchant_main);
            viewHolder.merchantBisName = (TextView) convertView.findViewById(R.id.product_name);
            viewHolder.merchantAddress = (TextView) convertView.findViewById(R.id.merchant_address);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(mtMerchants!=null) {
            if (mtMerchants.size() > 0) {
                MTMerchant merchant = mtMerchants.get(i);
                viewHolder.merchantBisName.setText(merchant.getMerchantFullBisName());
                viewHolder.merchantAddress.setText(merchant.getMerchantStreet());
                Bitmap image = imageCacheInterface.getImageFromCache(merchant.getMerchantLogoUrl());
                if(image==null) {
                    viewHolder.merchantLogo.setImageBitmap(preDetermineImageSize());
                    new MTImageLoader(context, viewHolder.merchantLogo, (ImageView)convertView.findViewById(R.id.image_loading_icon), this.imageCacheInterface).execute(merchant.getMerchantLogoUrl());
                }else{
                    renderDownloadedImageToView(image, viewHolder.merchantLogo);
                }
//                BitmapDrawable bitmapDrawable = (BitmapDrawable) viewHolder.merchantLogo.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                bitmap = MTImageHelper.createRequiredBitmap(bitmap);
//                bitmap = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
//                viewHolder.merchantLogo.setImageBitmap(bitmap);
                View views[] = {viewHolder.merchantAddress};
                FontStyleFactory.setFontForAllViews(context, views, StyleConstants.GENERAL_APP_FONT);
                FontStyleFactory.setFontForView(context, viewHolder.merchantBisName, StyleConstants.GENERAL_APP_FONT_BOLD);
            }
        }
        return convertView;
    }

    public Bitmap preDetermineImageSize(){
        Bitmap bitmap = Bitmap.createBitmap(1067,867, Bitmap.Config.ARGB_8888);
        bitmap = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
        Canvas canvas = new Canvas();
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        canvas.drawBitmap(bitmap,bitmap.getWidth(),bitmap.getHeight(),paint);
        //canvas.
        return bitmap;
    }

    private void renderDownloadedImageToView(Bitmap bitmap, final ImageView image){
        //bitmap = MTImageHelper.createRequiredBitmap(bitmap);//convert the image to a
        image.setVisibility(View.VISIBLE);
        Bitmap screenImage = MTImageHelper.createScreenFitBitmap(this.context, bitmap);
        image.setImageBitmap(screenImage);
    }

    static class ViewHolder
    {
        //Declare our components here.
        ImageView merchantLogo;
        TextView merchantBisName;
        TextView merchantAddress;
    }
}
