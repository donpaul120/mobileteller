package com.nnamdi.mobileteller.Merchant.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nnamdi.mobileteller.Factory.ViewFactory.FontStyleFactory;
import com.nnamdi.mobileteller.Factory.ViewFactory.StyleConstants;
import com.nnamdi.mobileteller.Merchant.Adapter.MerchantProductCategoryAdapter;
import com.nnamdi.mobileteller.Merchant.Interface.MTMerchantCategoryCallBackInterface;
import com.nnamdi.mobileteller.Merchant.MTMerchantDataKey;
import com.nnamdi.mobileteller.Merchant.Utility.MerchantProductCategory;
import com.nnamdi.mobileteller.Products.Activity.MTProductsActivity;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MerchantProductCategoriesFragment extends Fragment implements ItemsListCallBackInterface{

    //public static String TAG = "MERCHANT_PRODUCT_CATEGORIES";

    private Context context;
    private ListView productCategoryListView;
    private TextView merchantHasNoCategory;
    private ArrayList<MerchantProductCategory> productCategories;
    private String categories;
    private String merhantID;
    private MTMerchantCategoryCallBackInterface categoryCallBackInterface;
    private MerchantProductCategoryAdapter productCategoryAdapter;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MerchantProductCategoriesFragment.
     */
    public static MerchantProductCategoriesFragment newInstance(String merchantId, String bundle) {
        MerchantProductCategoriesFragment fragment = new MerchantProductCategoriesFragment();
        Bundle args = new Bundle();
        args.putString(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES, bundle);
        args.putString(MTMerchantDataKey.MERCHANT_ID, merchantId);
        fragment.setArguments(args);
        return fragment;
    }


    public MerchantProductCategoriesFragment() {
        // Required empty public constructor
    }


    private void initializeListView(){
        if(productCategories==null){
            productCategories = new ArrayList<>();
        }
        if(categories!=null){
            productCategories = getProductCategories(categories);
            onMerchantHasNoCategory();
        }
        productCategoryAdapter = new MerchantProductCategoryAdapter(context, productCategories);
        onCategoryClicked();
        productCategoryListView.setAdapter(productCategoryAdapter);
    }

    public void onCategoryClicked(){
        productCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MerchantProductCategory category = (MerchantProductCategory) productCategoryListView.getItemAtPosition(position);
                Intent intent = new Intent(context, MTProductsActivity.class);
                intent.putExtra("CATEGORY",category);
                intent.putExtra(MTMerchantDataKey.MERCHANT_ID, merhantID);
                startActivity(intent);
            }
        });
    }

    public void onMerchantHasNoCategory(){
        if(productCategories.size()==0){
            merchantHasNoCategory.setText("We Currently have no products!!!");
            merchantHasNoCategory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().containsKey(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES)) {
                categories = getArguments().getString(MTMerchantDataKey.MERCHANT_PRODUCT_CATEGORIES);
                merhantID = getArguments().getString(MTMerchantDataKey.MERCHANT_ID);
            }
        }
        this.context = getActivity();
    }


    private ArrayList<MerchantProductCategory> getProductCategories(String merchantProductCategoriesJson){

        ArrayList<MerchantProductCategory> productCategories = null;
        try{
            JSONArray categories = new JSONArray(merchantProductCategoriesJson);
            productCategories = new ArrayList<>();
            for (int i=0;i<categories.length();i++){
                JSONObject categoryObj = categories.getJSONObject(i);
                MerchantProductCategory category = new MerchantProductCategory();
                category.setCategoryName(categoryObj.getString(MTMerchantDataKey.CATEGORY_NAME));
                category.setCategoryDescription(categoryObj.getString(MTMerchantDataKey.CATEGORY_DESC));
                category.setCategoryId(categoryObj.getString(MTMerchantDataKey.CATEGORY_ID));
                productCategories.add(category);
            }
        }catch(JSONException ex){
            ex.printStackTrace();
        }
        return productCategories;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mercant_product_categories, container, false);
        setMerchantHasNoCategory((TextView) rootView.findViewById(R.id.merchant_no_category));
        productCategoryListView = (ListView) rootView.findViewById(R.id.category_list);
        productCategoryListView.setDivider(new ColorDrawable(Color.parseColor("#e7e7e7")));
        productCategoryListView.setDividerHeight(1);
        initializeListView();
        FontStyleFactory.setFontForView(context, merchantHasNoCategory, StyleConstants.GENERAL_APP_FONT_BOLD);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            categoryCallBackInterface = (MTMerchantCategoryCallBackInterface) activity;
        }catch(ClassCastException cl){
            cl.printStackTrace();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState==null){
            if(categoryCallBackInterface!=null){
                categoryCallBackInterface.requestForCategoryProductsCount(this, merhantID);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void onCategoriesCountReceived(ArrayList<MerchantProductCategory> categories){
        ArrayList<MerchantProductCategory> newCategoryList = new ArrayList<>();
        for (int i=0;i<categories.size();i++){
            MerchantProductCategory category = categories.get(i);
            String id = category.getCategoryId();
            for (int j=0;j<productCategories.size();j++){
                MerchantProductCategory category1 = productCategories.get(j);
                if(category1.getCategoryId().equalsIgnoreCase(id)){
                    category1.setCategoryProductCount(category.getCategoryProductCount());
                }
                newCategoryList.add(category1);
            }
        }
        this.productCategories = newCategoryList;
        productCategoryAdapter.notifyDataSetChanged();
    }

    public void setMerchantHasNoCategory(TextView merchantHasNoCategory) {
        this.merchantHasNoCategory = merchantHasNoCategory;
    }

    @Override
    public void onConnectionFailed() {
        Toast.makeText(context, "faileddd..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuccessful(ArrayList<?> items) {
        if(items!=null){
            @SuppressWarnings("unchecked")
            ArrayList<MerchantProductCategory> categories = (ArrayList<MerchantProductCategory>) items;
            onCategoriesCountReceived(categories);
        }
    }
}