package com.nnamdi.mobileteller.Merchant.Utility;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author  Paul Okeke
 * Created by paulex10 on 16/06/2015.
 */
public class MerchantProductCategory implements Parcelable{

    private String categoryName;
    private String categoryId;
    private String categoryDescription;
    private int categoryIconId;
    private int categoryProductCount;

    public MerchantProductCategory(){
        //default
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static Creator<MerchantProductCategory> CREATOR = new Creator<MerchantProductCategory>() {
        @Override
        public MerchantProductCategory createFromParcel(Parcel source) {
            return new MerchantProductCategory(source);
        }

        @Override
        public MerchantProductCategory[] newArray(int size) {
            return new MerchantProductCategory[size];
        }
    };

    public MerchantProductCategory(Parcel parcel){
        categoryName = parcel.readString();
        categoryId = parcel.readString();
        categoryDescription = parcel.readString();
        categoryIconId = parcel.readInt();
        categoryProductCount = parcel.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryName);
        dest.writeString(categoryId);
        dest.writeString(categoryDescription);
        dest.writeInt(categoryIconId);
        dest.writeInt(categoryProductCount);
    }

    public int getCategoryIconId() {
        return categoryIconId;
    }

    public void setCategoryIconId(int categoryIconId) {
        this.categoryIconId = categoryIconId;
    }

    public int getCategoryProductCount() {
        return categoryProductCount;
    }

    public void setCategoryProductCount(int categoryProductCount) {
        this.categoryProductCount = categoryProductCount;
    }
}
