package com.nnamdi.mobileteller.Merchant.Activity;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.Merchant.Fragment.MerchantDetailFragment;
import com.nnamdi.mobileteller.Merchant.Fragment.MerchantProductCategoriesFragment;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Merchant.Interface.MTMerchantCategoryCallBackInterface;
import com.nnamdi.mobileteller.Products.Fragment.RequestFragment;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;


public class MerchantDetailActivity extends AppCompatActivity implements ItemsListCallBackInterface, MTMerchantCategoryCallBackInterface {

    private FragmentManager manager;
    private ItemsListCallBackInterface itemsListCallBackInterface;
    private RequestFragment requestFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_product_categories);
        manager = getSupportFragmentManager();
        if(savedInstanceState==null){
            attachFragment();
        }
        attachRequestFragment();
        setToolBar();
    }

    public void setToolBar(){
        Toolbar toolbar = MTGeneralActionBar.setGeneralActionBar(this, "", "");
        if(toolbar!=null){toolbar.bringToFront();
            toolbar.setBackgroundColor(Color.TRANSPARENT);}
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void attachFragment(){
        MerchantDetailFragment merchantDetailFragment = (MerchantDetailFragment)
                manager.findFragmentByTag(MerchantDetailFragment.TAG);
        if(merchantDetailFragment==null){
            manager.beginTransaction().add(R.id.container,
                    MerchantDetailFragment.newInstance(getIntent().getExtras()),
                    MerchantDetailFragment.TAG)
                    .commit();
        }
    }

    public void attachRequestFragment(){
        requestFragment = (RequestFragment) manager.findFragmentByTag(RequestFragment.TAG);
        if(requestFragment==null){
            requestFragment = new RequestFragment();
            manager.beginTransaction()
                    .add(requestFragment, RequestFragment.TAG)
                    .commit();
        }
    }

    public void requestForCategoryProductsCount(ItemsListCallBackInterface callback, String merchantId){
        if(requestFragment!=null){
            itemsListCallBackInterface = callback;
            requestFragment.requestCategoryProductsCount(merchantId);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_merchant_product_categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnectionFailed() {
        itemsListCallBackInterface.onConnectionFailed();
    }

    @Override
    public void onConnectionSuccessful(ArrayList<?> items) {
        itemsListCallBackInterface.onConnectionSuccessful(items);
    }
}
