package com.nnamdi.mobileteller.Merchant.Activity;



import android.app.SearchManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nnamdi.mobileteller.Components.MTListView;
import com.nnamdi.mobileteller.Components.MTDrawerLayout;
import com.nnamdi.mobileteller.Components.MTGeneralActionBar;
import com.nnamdi.mobileteller.ConnectionPool.Interfaces.ImageCacheInterface;
import com.nnamdi.mobileteller.Constants.ActionType;
import com.nnamdi.mobileteller.Merchant.Fragment.MTMerchantFragment;
import com.nnamdi.mobileteller.Merchant.Interface.MTFragmentCommunicationInterface;
import com.nnamdi.mobileteller.Products.Fragment.RequestFragment;
import com.nnamdi.mobileteller.Products.Interface.ItemsListCallBackInterface;
import com.nnamdi.mobileteller.R;

import java.util.ArrayList;

public class MTMerchantActivity extends AppCompatActivity implements MTFragmentCommunicationInterface, ItemsListCallBackInterface, ImageCacheInterface {


    private static final String CONNECTION_ERROR_VISIBILITY = "CONNECTION_ERROR_VISIBILITY";

    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ImageView loadingFragmentIcon;
    private TextView connectionStatusView;
    private Button connectionRetryBtn;
    private Animation rotate;
    private MTMerchantFragment merchantFragment;
    private LinearLayout connectionStatusContainer;
    private FragmentManager fragmentManager;
    private ItemsListCallBackInterface merchantCallBackInterface;
    private RequestFragment requestFragment;
    private LruCache<String, Bitmap> merchantsImageCache;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchants);

        toolbar = MTGeneralActionBar.setGeneralActionBar(this,"MobileTeller","Merchants");
        this.setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();
        attachRequestFragment();
        addFragmentToActivity();
        initProductImageCache();
        if(savedInstanceState==null){
            requestMerchantList();
        }
        int listViewID = R.id.mobileTelListV;
        int drawerID = R.id.myDrawer;
        MTDrawerLayout MTDrawerLayout = new MTDrawerLayout(this, toolbar, drawerID, listViewID);
        actionBarDrawerToggle = MTDrawerLayout.getActionBarDrawerToggle();
        //Widget Setup
        setConnectionStatusContainer();
        setConnectionRetryBtn();
        setConnectionStatusView();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(connectionStatusContainer!=null){
            outState.putInt(CONNECTION_ERROR_VISIBILITY, connectionStatusContainer.getVisibility());
        }
    }

    @Override
    protected void onRestoreInstanceState(@Nullable Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            //
            connectionStatusContainer.setVisibility(savedInstanceState.getInt(CONNECTION_ERROR_VISIBILITY));
        }
    }


    /**
     * MobileTeller
     * Paul Okeke
     * Triggers the background process to request
     * for list of merchants
     * @since 2014
     */
    public void requestMerchantList()
    {
        requestFragment.requestMerchantsByLocation();
        startLoadingMerchantsAnim();
    }

    public void initProductImageCache(){
        if(requestFragment!=null){
            merchantsImageCache = requestFragment.bitmapLruCache;
            if(merchantsImageCache == null){
                int maxMemory = (int) (Runtime.getRuntime().maxMemory()/1024);
                int freeMemory = (int) (Runtime.getRuntime().freeMemory()/1024);
                //Toast.makeText(this, "MAX::"+maxMemory, Toast.LENGTH_SHORT).show();
                //Toast.makeText(this,"FREE ::"+freeMemory, Toast.LENGTH_LONG).show();
                int cacheSize = maxMemory/8;
                //Toast.makeText(this,"CACHE SIZE ::"+cacheSize, Toast.LENGTH_LONG).show();
                merchantsImageCache = new LruCache<String, Bitmap>(cacheSize){
                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getRowBytes()/1024;
                    }
                };
            }
            requestFragment.bitmapLruCache = merchantsImageCache;
        }
    }

    /**
     * MobileTeller
     */
    public void startLoadingMerchantsAnim()
    {
        loadingFragmentIcon = (ImageView) findViewById(R.id.load);
        loadingFragmentIcon.setVisibility(View.VISIBLE);
        rotate = AnimationUtils.loadAnimation(this, R.anim.mt_activity_loading);
        loadingFragmentIcon.startAnimation(rotate);
    }

    public void stopLoadingMerchantAnim()
    {
        rotate.cancel();
        loadingFragmentIcon.clearAnimation();
        loadingFragmentIcon.setVisibility(View.GONE);
    }

    public void setConnectionRetryBtn()
    {
        connectionRetryBtn = (Button) findViewById(R.id.conn_retry);
        connectionRetryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = new AlphaAnimation(1, 0);
                animation.setInterpolator(new AccelerateDecelerateInterpolator());
                animation.setDuration(500);
                connectionStatusContainer.startAnimation(animation);
                connectionStatusContainer.setVisibility(View.GONE);
                requestMerchantList();
            }
        });
    }

    public void  setConnectionStatusView()
    {
        connectionStatusView = (TextView) findViewById(R.id.conn_stat);
    }

    public void displayConnectionError()
    {
        stopLoadingMerchantAnim();
        Animation animation = new AlphaAnimation(0, 1);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(500);
        connectionStatusContainer.startAnimation(animation);
        connectionStatusContainer.setVisibility(View.VISIBLE);
    }

    /**
     * MobileTeller
     */
    public void addFragmentToActivity()
    {
        merchantFragment = (MTMerchantFragment) fragmentManager.findFragmentByTag(MTMerchantFragment.TAG);
        if(merchantFragment ==null) {
            merchantFragment = MTMerchantFragment.newInstance(null);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, merchantFragment, MTMerchantFragment.TAG)
                    .commit();
        }
        merchantCallBackInterface = merchantFragment;
    }

    public void attachRequestFragment() {
        requestFragment = (RequestFragment) fragmentManager.findFragmentByTag(RequestFragment.TAG);
        if (requestFragment == null) {
            requestFragment = new RequestFragment();
            fragmentManager.beginTransaction()
                    .add(requestFragment, RequestFragment.TAG)
                    .commit();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.product, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView( menu.findItem(R.id.action_settings));

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return actionBarDrawerToggle.onOptionsItemSelected(item)
                || id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    boolean isShown = false;
    @Override
    public void triggerAction(int ActionType,int id, Bundle data) {
        //Do Something here brother
        if(ActionType== com.nnamdi.mobileteller.Constants.ActionType.LIST_SCROLL) {
            MTGeneralActionBar.animateToolbarOnListScroll(data.getInt(MTListView.SCROLL_POSITION), toolbar, this);
        }
        if(ActionType == com.nnamdi.mobileteller.Constants.ActionType.LIST_DATA_REFRESH){
            requestMerchantList();
            stopLoadingMerchantAnim();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        moveTaskToBack(true);
    }

    public void setConnectionStatusContainer() {
        this.connectionStatusContainer = (LinearLayout) findViewById(R.id.connection_status_container);
        //ImageView error = (ImageView) findViewById(R.id.netowrk_error_icon);
    }

    @Override
    public void onConnectionFailed() {
        displayConnectionError();
    }

    @Override
    public void onConnectionSuccessful(ArrayList<?> items) {
        merchantCallBackInterface.onConnectionSuccessful(items);
        stopLoadingMerchantAnim();
    }

    @Override
    public void addToCache(String urlKey, Bitmap image) {
        if(merchantsImageCache!=null){
            if(merchantsImageCache.get(urlKey)==null) {
                merchantsImageCache.put(urlKey, image);
            }
        }
    }

    @Override
    public Bitmap getImageFromCache(String urlKey) {
        return merchantsImageCache.get(urlKey);
    }
}